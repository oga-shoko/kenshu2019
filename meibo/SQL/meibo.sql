-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- ホスト: 127.0.0.1
-- 生成日時: 
-- サーバのバージョン： 10.4.6-MariaDB
-- PHP のバージョン: 7.3.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- データベース: `meibo`
--

-- --------------------------------------------------------

--
-- テーブルの構造 `grade_master`
--

CREATE TABLE `grade_master` (
  `grade_id` int(1) NOT NULL,
  `grade_name` varchar(10) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- テーブルのデータのダンプ `grade_master`
--

INSERT INTO `grade_master` (`grade_id`, `grade_name`) VALUES
(1, '部長'),
(2, '課長'),
(3, 'リーダー'),
(4, 'メンバー');

-- --------------------------------------------------------

--
-- テーブルの構造 `member`
--

CREATE TABLE `member` (
  `staff_id` int(3) NOT NULL,
  `name` varchar(20) COLLATE utf8_bin NOT NULL,
  `birthplace` int(2) NOT NULL,
  `gender` int(1) NOT NULL,
  `age` int(2) NOT NULL,
  `section_id` int(1) NOT NULL,
  `grade_id` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- テーブルのデータのダンプ `member`
--

INSERT INTO `member` (`staff_id`, `name`, `birthplace`, `gender`, `age`, `section_id`, `grade_id`) VALUES
(1, '鈴木一郎', 2, 1, 30, 1, 1),
(21, '山田太郎', 3, 1, 44, 2, 3);

-- --------------------------------------------------------

--
-- テーブルの構造 `section_master`
--

CREATE TABLE `section_master` (
  `section_id` int(1) NOT NULL,
  `section_name` varchar(10) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- テーブルのデータのダンプ `section_master`
--

INSERT INTO `section_master` (`section_id`, `section_name`) VALUES
(1, '第一事業部'),
(2, '第二事業部'),
(3, '営業'),
(4, '総務'),
(5, '人事');

--
-- ダンプしたテーブルのインデックス
--

--
-- テーブルのインデックス `member`
--
ALTER TABLE `member`
  ADD PRIMARY KEY (`staff_id`);

--
-- ダンプしたテーブルのAUTO_INCREMENT
--

--
-- テーブルのAUTO_INCREMENT `member`
--
ALTER TABLE `member`
  MODIFY `staff_id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
