<!DOCTYPE html>
<?php

   include("./functions.php");
   $pdo = initDB();


//   $query_str = "SELECT * FROM test WHERE ryourimei LIKE '%" . $_GET['namae'] . "%'" and genre = $_GET['genre'] and price = $_GET['price'];
    $where_str  = "";
    $cond_name = "";
    $cond_gender = "";
    $cond_section = "";
    $cond_grade = "";

    if(isset($_GET['name']) && !empty($_GET['name'])){
      $where_str .=  " AND m.name LIKE '%" . $_GET['name'] . "%'";
      $cond_name = $_GET['name'];
    }

    if(isset($_GET['gender']) && !empty($_GET['gender'])){
      $where_str .= " AND m.gender = '" . $_GET['gender'] . "'";
      $cond_gender = $_GET['gender'];
    }

    if(isset($_GET['section']) && !empty($_GET['section'])){
      $where_str .= " AND sm.section_id = '" . $_GET['section'] . "'";
      $cond_section = $_GET['section'];

    }

    if(isset($_GET['grade']) && !empty($_GET['grade'])){
      $where_str .= " AND gm.grade_id = '" . $_GET['grade'] . "'";
      $cond_grade = $_GET['grade'];
    }

  //  if($where_str != ""){
    //  $where_str = " WHERE " . substr($where_str,0,-4);
//    }

    $query_str = "SELECT
                  m.staff_id,
                  m.name,
                  sm.section_name,
                  gm.grade_name
                  FROM member AS m
                  LEFT JOIN section_master AS sm ON sm.section_id=m.section_id
                  LEFT JOIN grade_master AS gm ON gm.grade_id=m.grade_id
                  WHERE 1=1";

    $query_str .= $where_str;
    $query_str .= " ORDER BY m.staff_id";

  //echo $query_str;
  $sql = $pdo->prepare($query_str);
  $sql->execute();
  $result = $sql->fetchAll();
  // SQLから返ってきたものを保存してる

 ?>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>名簿システム</title>
    <style>

      table{
        margin: auto;
      }

      table th{
        background-color: gray;
        color: white;
        width: 150px;
        height: auto;
      }
      table td{
        width: auto;
        height: 50px;
        border-left: 2px solid gray;
        border-bottom: 2px solid gray;
        border-top:none;
        text-align: center;
      }
      table td:last-child{
        border-right: 2px solid gray;
      }

      .result{
        margin-left: 314.667px;
      }
    </style>

    </table>

    <script type="text/javascript">
    function clearForm(){
      document.searchform.name.value="";
      document.searchform.gender.value="";
      document.searchform.section.value="";
      document.searchform.grade.value="";
    }

    function searchResult(){
      document.getElementsByID('className')
    }

  </script>

  </head>
  <body>
    <?php include( dirname(__FILE__) . './header.php'); ?>
    <pre>
    <?php
    //var_dump($result);
     ?>
   </pre>
    <div class="form" style="text-align: center">
      <form method="get" action="index.php" name="searchform">
        名前：
        <input type="text" name="name" value ="<?php echo $cond_name; ?>">
        <br/>
        性別：
        <select name="gender">
              <option value="" <?php if($cond_gender==""){echo "selected";}?>>すべて</option>
              <option value="1" <?php if($cond_gender=="1"){echo "selected";}?>>男性</option>
              <option value="2" <?php if($cond_gender=="2"){echo "selected";}?>>女性</option>
            </select>
          部署：
          <select name="section">
            <option value="" <?php if($cond_section==""){echo "selected";}?>>すべて</option>
            <option value="1" <?php if($cond_section=="1"){echo "selected";}?>>第一事業部</option>
            <option value="2" <?php if($cond_section=="2"){echo "selected";}?>>第二事業部</option>
            <option value="3" <?php if($cond_section=="3"){echo "selected";}?>>営業</option>
            <option value="4" <?php if($cond_section=="4"){echo "selected";}?>>総務</option>
            <option value="5" <?php if($cond_section=="5"){echo "selected";}?>>人事</option>
          </select>
          役職：
          <select name="grade">
              <option value="" <?php if($cond_grade==""){echo "selected";}?>>すべて</option>
              <option value="1" <?php if($cond_grade=="1"){echo "selected";}?>>部長</option>
              <option value="2" <?php if($cond_grade=="2"){echo "selected";}?>>課長</option>
              <option value="3" <?php if($cond_grade=="3"){echo "selected";}?>>リーダー</option>
              <option value="4" <?php if($cond_grade=="4"){echo "selected";}?>>メンバー</option>
          </select>
            <br/>
            <input type="submit" onClick="searchResult();">
            <input type="button" value="リセット" onClick="clearForm();">
          </form>
        </div>

          <hr>
    <pre>
      <?php
      // var_dump($result);
       ?>
    </pre>
      <div class="searchResult">
        <div class="result">検索結果：<?php echo count($result); ?></div>

      <table>
      <tr>
        <th>社員ID</th>
        <th>名前</th>
        <th>部署</th>
        <th>役職</th>
      </tr>
      <tr>
      <?php
        foreach ($result as $each){
      ?>
        <td><?php echo $each['staff_id'];?></td>
        <td><a href="./detail01.php?staff_id=<?php echo $each['staff_id'];?>"><?php echo $each['name'];?></a></td>
        <td><?php echo $each['section_name'];?></td>
        <td><?php echo $each['grade_name'];?></td>
        </tr>
        <?php } ?>
    </table>
  </div>
  </body>
</html>
