<!DOCTYPE html>
<html>
<!DOCTYPE html>
<?php
  include("./functions.php");
  include("./statics.php");
  ?>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
      .button{
        text-align : right ;
        }

        table{
          margin: auto;
        }

        table th{
          background-color: gray;
          color: white;
          width: 150px;
          height: auto;
        }
        table td{
          width: 600px;
          height: 50px;
          border-left:none;
          border-right: 2px solid gray;
          border-bottom: 2px solid gray;
        }

        .first{
          border-top: 2px solid gray;
        }

    </style>
    <script type="text/javascript">
    function entry(){
      if(window.confirm('登録してよろしいですか？')){
	       return true;
       }
       else{
         window.alert('キャンセルされました');
         return false;
       }
     }
   </script>
    <title>名簿システム</title>
  </head>
  <body>
    <?php include( dirname(__FILE__) . './header.php'); ?>
    <div class="form">
      <form method="post" action="entry02.php">
        <table>
          <tr>
            <th>
              名前
            </th>
            <td class="first">
              <input type="text" name="name" value ="">
            </td>
          </tr>
          <tr>
            <th>
              出身地
            </th>
            <td>
              <select name="birthplace">
                <?php
                foreach($prefecture_array as $key => $value){
                  echo "<option value='" . $key . "'>" . $value . "</option>";
                }
                ?>

            </td>
          </tr>
          <tr>
            <th>
              性別
            </th>
            <td>
              <input type="radio" name="gender" value="1" checked="checked">男性
              <input type="radio" name="gender" value="2">女性
            </td>
          </tr>
          <tr>
            <th>
              年齢
            </th>
            <td>
              <input type="text" name="age" value ="">
            </td>
          </tr>
          <tr>
            <th>
              部署
            </th>
            <td>
              <input type="radio" name="section" value="1" checked="checked">第一事業部
              <input type="radio" name="section" value="2">第二事業部
              <input type="radio" name="section" value="3">営業
              <input type="radio" name="section" value="4">総務
              <input type="radio" name="section" value="5">人事
            </td>
          </tr>
          <tr>
            <th>
              役職
            </th>
            <td>
              <input type="radio" name="grade" value="1" checked="checked">部長
              <input type="radio" name="sgrade" value="2">課長
              <input type="radio" name="grade" value="3">リーダー
              <input type="radio" name="grade" value="4">メンバー
            </td>
          </tr>
        </table>
        <div class="button">
          <input type="submit"  value="登録" onclick="entry()">
          <input type="reset">
        </form>
      </div>
      </div>
    </body>
</html>
