$(function(){

	//movie高さ調整
	youtube_adjust_height();

	//■リサイズ完了時の処理
	var timer = false;
	$(window).resize(function() {
	    if (timer !== false) {
	        clearTimeout(timer);
	    }
	    timer = setTimeout(function() {

				youtube_adjust_height();

	    }, 200);
	});



	function youtube_adjust_height(){

		movie_height = Math.floor( $('.adjust_height').width() / 10 * 6 );

		$('.adjust_height').height(movie_height);
	}

});
