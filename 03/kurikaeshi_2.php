<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>繰り返し</title>
    <link rel="stylesheet" href="style.css">
  </head>
  <body>
    <form method="get" action="kurikaeshi_2.php">
        <input type="text" name="cols">
        行のテーブルを生成する
        <input type="submit">
        <input type="reset">
    </form>

    <hr>

    <table border="1">
      <?php
      if(isset($_GET['cols'])){
        for($i=0; $i < $_GET['cols']; $i++){
          if ($i % 2 == 0) {
            echo
            '<tr>
            <td style="background-color:#21BDDE">赤</td>
            <td style="background-color:#21BDDE">青</td>
            <td style="background-color:#21BDDE">黄</td>
            </tr>';
            } else {
              echo
              '<tr>
              <td style="background-color:#f5f5f5">赤</td>
              <td style="background-color:#f5f5f5">青</td>
              <td style="background-color:#f5f5f5">黄</td>
              </tr>';
            }
          }
        }
       ?>
     </table>
   </body>
</html>
