
$(function(){

	//PC・スマホ共通

	//■文字数制限
    var $setElm = $('.lead');
    var cutFigure = '70'; // カットする文字数
    var afterTxt = '…'; // 文字カット後に表示するテキスト

    $setElm.each(function(){
        var textLength = $(this).text().length;
        var textTrim = $(this).text().substr(0,(cutFigure))
        var textTrim2 = $(this).text().substr(0,(cutFigure - 1))

        if(cutFigure < textLength) {
            $(this).html(textTrim2 + afterTxt).css({visibility:'visible'});
        } else if(cutFigure >= textLength) {
            $(this).css({visibility:'visible'});
        }
    });

	//■するするスクロール
  $("a[href^='#']").click(function(){
      var speed = 500;
      var href= $(this).attr("href");
      var target = $(href == "#" || href == "" ? 'html' : href);
      var position = target.offset().top;

			if( $(this).attr('href') == '#page_top'){
	      $("html, body").animate({scrollTop:0}, speed, "swing");
			}else{
	      $("html, body").animate({scrollTop:position}, speed, "swing");
			}


      return false;
  });


	//■メニューにアクティブ付与
	if( $('body').attr('id') === 'page-the_edge-list' ){		//記事一覧ページ
		$('header .bg_gray ul > li').eq(1).addClass('active').find('a').attr('href', "javascript:void(0);");
	}

	//■記事にリンク付与
	$('.jump_to_article').click(function(){
		linkto = $(this).attr('linkto');
		window.location.href = linkto;
	});

	$('.jump_to').click(function(e){
	    e.stopPropagation();
	    e.preventDefault();
	    var linkto = $(this).attr('linkto');
	    window.location.href = linkto;
	});
	$('.open_to').click(function(e){
	    e.stopPropagation();
	    e.preventDefault();
	    var linkto = $(this).attr('linkto');
	    window.open(linkto, '_blank');
	});

	//■NEW表示処理
	period_new = 7;	//14日以内ならNew表示

	last_update = $('header.main .last_update').html().substring(0,10).replace(/\./g,"/");

	//それぞれの処理
	$('article .date').each(function(){

		//記事の最新日テキストを取得
		article_update = $(this).html().substring(0,10).replace(/\./g,"/");

		if( period_new < getDiff(article_update, last_update) ){
			$(this).closest('article').find('.new').hide();
		}

	});



	//スマホのとき
	if( window.innerWidth < 768){

		//■メニュー開くボタン処理
		$('header .sp_icon_menu a').click(open_menu);

		//■メニュー閉じるボタン処理
		$('header .menu_btn_close a').click(close_menu);
		$('header div.bg_gray').click(close_menu);

		//■ヘッダ固定化
		$('header').css('position','fixed');
		$('header').css('top','0px');
		$('#content').css('margin-top','105px');

	}else if( window.innerWidth <= 1120 ) {

		//■ヘッダ固定化
		$('header').css('position','fixed');
		$('header').css('top','0px');
		$('#content').css('margin-top','179px');

	}else{	//PCのときのみ

		//■ヘッダ固定化
		$('header').css('position','fixed');
		$('header').css('top','0px');
		$('#content').css('margin-top','109px');

	}



	//初回ロード時にウィンドウサイズを記録
	w_wid = window.innerWidth;
	$('body').attr('w', w_wid);

	//■リサイズ完了時の処理
	var timer = false;
	$(window).resize(function() {

			//幅リサイズすると、強制リセット
			if( $('body').attr('w') !=  window.innerWidth){

				/*if( $('header .sp_icon_menu').hasClass('locked') || $('header .sp_icon_menu').hasClass('st_open') ){*/
					reset_menu();
				/*}*/

			}

			//ヘッダ余白の調整
			if( window.innerWidth > 1120 ){	//PC
				$('#content').css('margin-top','109px');
			}else if( window.innerWidth >= 768 ){	//タブレット
				$('#content').css('margin-top','179px');
			}else{	//SP
				$('#content').css('margin-top','105px');
			}

	    if (timer !== false) {
	        clearTimeout(timer);
	    }
	    timer = setTimeout(function() {

//	        console.log('resized');

					w_old = Number( $('body').attr('w') );

					if( w_old < 768){	//もともとスマホサイズ

						if( window.innerWidth >= 768 ){	//リサイズ後はPCサイズ

							//リサイズ（SP → PC）

							//■メニュー開くボタン処理削除
							$('header .sp_icon_menu a').unbind();

							//■メニュー閉じるボタン処理削除
							$('header .menu_btn_close a').unbind();

							//リサイズ後の幅を記録
							$('body').attr('w', window.innerWidth );

						}
					}else{	//もともとPCサイズ
						if( window.innerWidth < 768 ){	//リサイズ後はスマホサイズ

							//リサイズ（PC → SP）

							//■メニュー開くボタン処理追加
							$('header .sp_icon_menu a').click(open_menu);

							//■メニュー閉じるボタン処理追加
							$('header .menu_btn_close a').click(close_menu);

							//リサイズ後の幅を記録
							$('body').attr('w', window.innerWidth );

						}
					}

	    }, 200);
	});



	function open_menu(){

		//ロックがONの時は何もしない
		if($('header .sp_icon_menu').hasClass('locked')){
			return false;
		}

		//ロックフラグON
		$('header .sp_icon_menu').addClass('locked');

		//閉じてるとき
		if( $('header .sp_icon_menu').hasClass('st_close') ){

			$('header .bg_gray').show().animate({
				right:"0%"
			},300,"swing",function(){

				//ステート変更
				$('header .sp_icon_menu').removeClass('st_close');
				$('header .sp_icon_menu').addClass('st_open');

//console.log( 'scroll:' + $(window).scrollTop() );

				//スクロール固定
				$('body').attr( 'MenuOpenPos', $(window).scrollTop() );	//スクロール位置の記憶
				$(window).scrollTop(0);

				//本文位置調整
				$('#content').css('margin-top', '-' + ($('body').attr( 'MenuOpenPos')-105) + 'px' );

				//全体の高さをメニューの高さに
				$('#page_top').css({ "overflow":"hidden", "height":$('header .bg_gray').height() });

				//ヘッダ固定解除
				$('header').css('position','absolute');

				//クローズボタンは固定化
				$('p.menu_btn_close').css( 'position','fixed' );
				//$('div#content').css( 'position','fixed' );



				//ロック解除
				$('header .sp_icon_menu').removeClass('locked');

			});
		}
	}



	function close_menu(){

		//ロックがONの時は何もしない
		if($('header .sp_icon_menu').hasClass('locked')){
			return false;
		}

		//開いているとき
		if( $('header .sp_icon_menu').hasClass('st_open') ){



			//スクロール固定解除

			//全体の高さを成り行きに
			$('#page_top').css({ "overflow":"visible", "height":"auto" });

			$(window).scrollTop( $('body').attr('MenuOpenPos') );

			//本文位置調整
			$('#content').css('margin-top', '105px' );

			//ヘッダ固定化
			$('header').css('position','fixed');

			//クローズボタンは固定解除
			$('p.menu_btn_close').css( 'position','static' );



			$('header .bg_gray').animate({
				right:"-100%"
			},300,"swing",function(){

				//ステート変更
				$('header .sp_icon_menu').removeClass('st_open');
				$('header .sp_icon_menu').addClass('st_close');

				//ロック解除
				$('header .sp_icon_menu').removeClass('locked');

				//メニューを非表示に
				$('header .bg_gray').hide()

			});
		}
	}



	function reset_menu(){

		//ロック削除
		if( $('header .sp_icon_menu').hasClass('locked') ){
			$('header .sp_icon_menu').removeClass('locked')
		}

		//st_open削除
		if( $('header .sp_icon_menu').hasClass('st_open') ){
			$('header .sp_icon_menu').removeClass('st_open')
		}

		//st_close付与
		if( !$('header .sp_icon_menu').hasClass('st_close') ){
			$('header .sp_icon_menu').addClass('st_close')
		}

		//style削除
		$('header .bg_gray').attr('style',"");



		//スクロール固定解除

		//全体の高さを成り行きに
		$('#page_top').css({ "overflow":"visible", "height":"auto" });

		$('body').removeAttr('MenuOpenPos');

		//ヘッダ固定化
		$('header').css('position','fixed');

		//クローズボタンは固定解除
		$('p.menu_btn_close').css( 'position','static' );

	}



	/*
	 *日付の差分日数を返却します。
	 */
	function getDiff(date1Str, date2Str) {
		var date1 = new Date(date1Str);
		var date2 = new Date(date2Str);

		// getTimeメソッドで経過ミリ秒を取得し、２つの日付の差を求める
		var msDiff = date2.getTime() - date1.getTime();

		// 求めた差分（ミリ秒）を日付へ変換します（経過ミリ秒÷(1000ミリ秒×60秒×60分×24時間)。端数切り捨て）
		var daysDiff = Math.floor(msDiff / (1000 * 60 * 60 *24));

		// 差分へ1日分加算して返却します
		return ++daysDiff;
	}

	//SNSボタン
  var shareTitle = encodeURI($('title').html());
  var shareDescription = encodeURI($('meta[name="description"]').attr('content'));
  var shareUrl = encodeURI(document.URL);
  var shareUrlComponent = encodeURIComponent(document.URL);


  $('.sns_links.js_func .twitter a').attr("href", "http://twitter.com/share?url="+ shareUrl + "&text=" + shareTitle);
  $('.sns_links.js_func .facebook a').attr("href", "http://www.facebook.com/sharer.php?u="+ shareUrl +"&t=" + shareTitle);
  $('.sns_links.js_func .in a').attr("href", "http://www.linkedin.com/shareArticle?mini=true&url=" + shareUrl + "&title=" + shareTitle);
  $('.sns_links.js_func .line a').attr("href", "http://line.me/R/msg/text/?" + shareDescription + " " + shareUrl );


});
