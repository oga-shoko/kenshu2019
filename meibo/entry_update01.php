<!DOCTYPE html>
<?php
  include("./functions.php");
  include("./statics.php");

  $pdo = initDB();

  if(isset($_POST['staff_id'])) { $param_id = $_POST['staff_id']; }
  $query_str = "SELECT
                m.staff_id,
                m.name,
                m.birthplace,
                m.gender,
                m.age,
                m.section_id,
                m.grade_id,
                sm.section_name,
                gm.grade_name
                FROM member AS m
                LEFT JOIN section_master AS sm ON sm.section_id=m.section_id
                LEFT JOIN grade_master AS gm ON gm.grade_id=m.grade_id
                WHERE staff_id = '".$param_id."'";


  $query_str;
  $sql = $pdo->prepare($query_str);
  $sql->execute();
  $result = $sql->fetchAll();
  // SQLから返ってきたものを保存してる

 ?>

<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
      .button{
        text-align : right ;
        }

        table{
          margin: auto;
        }

        table th{
          background-color: gray;
          color: white;
          width: 150px;
          height: auto;
        }
        table td{
          width: 600px;
          height: 50px;
          border-left:none;
          border-right: 2px solid gray;
          border-bottom: 2px solid gray;
        }

        .first{
          border-top: 2px solid gray;
        }

    </style>
    <script type="text/javascript">
    function update(){
      if(window.confirm('更新してよろしいですか？')){
         return true;
       }
       else{
         window.alert('キャンセルされました');
         return false;
       }
     }
   </script>
    <title>名簿システム</title>
  </head>
  <body>
    <?php include( dirname(__FILE__) . './header.php'); ?>
    <div class="form">
      <form method="post" action="entry_update02.php">
        <table>
          <tr>
            <th>
              社員ID
            </th>
            <td class="first">
              <?php echo $result[0]['staff_id'];?>
            </td>
          </tr>
          <tr>
            <th>
              名前
            </th>
            <td>
              <input type="text" name="name" value ="<?php echo $result[0]['name'];?>">
            </td>
          </tr>
          <tr>
            <th>
              出身地
            </th>
            <td>
              <select name="birthplace"  required>
                <?php
                foreach($prefecture_array as $key => $value){
                  echo "<option value='" . $key . "'";
                  if($result[0]['birthplace'] == $key){ echo " selected ";}
                   echo ">" . $value . "</option>";
                }
                ?>

            </td>
          </tr>
          <tr>
            <th>
              性別
            </th>
            <td>
              <input type="radio" name="gender" value="1" <?php if($result[0]['gender']=="1"){echo "checked";}?>>男性
              <input type="radio" name="gender" value="2" <?php if($result[0]['gender']=="2"){echo "checked";}?>>女性
            </td>
          </tr>
          <tr>
            <th>
              年齢
            </th>
            <td>
              <input type="text" name="age" value ="<?php echo $result[0]['age'];?>">
            </td>
          </tr>
          <tr>
            <th>
              部署
            </th>
            <td>
              <input type="radio" name="section" value="1" <?php if($result[0]['section_id']=="1"){echo "checked";}?>>第一事業部
              <input type="radio" name="section" value="2" <?php if($result[0]['section_id']=="2"){echo "checked";}?>>第二事業部
              <input type="radio" name="section" value="3" <?php if($result[0]['section_id']=="3"){echo "checked";}?>>営業
              <input type="radio" name="section" value="4" <?php if($result[0]['section_id']=="4"){echo "checked";}?>>総務
              <input type="radio" name="section" value="5" <?php if($result[0]['section_id']=="5"){echo "checked";}?>>人事
            </td>
          </tr>
          <tr>
            <th>
              役職
            </th>
            <td>
              <input type="radio" name="grade" value="1" <?php if($result[0]['grade_id']=="1"){echo "checked";}?>>部長
              <input type="radio" name="grade" value="2" <?php if($result[0]['grade_id']=="2"){echo "checked";}?>>課長
              <input type="radio" name="grade" value="3" <?php if($result[0]['grade_id']=="3"){echo "checked";}?>>リーダー
              <input type="radio" name="grade" value="4" <?php if($result[0]['grade_id']=="4"){echo "checked";}?>>メンバー
            </td>
          </tr>
        </table>
        <input type="hidden" name="id" value ="<?php echo $result[0]['staff_id'];?>">
        <div class="button">
          <input type="submit" value="更新" onclick="update()">
          <input type="reset">
        </form>
      </div>
    </body>
</html>
