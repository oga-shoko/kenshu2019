(function(){
  !function(t,e){if(void 0===e[t]){e[t]=function(){e[t].clients.push(this),this._init=[Array.prototype.slice.call(arguments)]},e[t].clients=[];for(var r=function(t){return function(){return this["_"+t]=this["_"+t]||[],this["_"+t].push(Array.prototype.slice.call(arguments)),this}},s=["blockEvents","unblockEvents","setSignedMode","setAnonymousMode","resetUUID","addRecord","fetchGlobalID","set","trackEvent","trackPageview","trackClicks","ready"],n=0;n<s.length;n++){var c=s[n];e[t].prototype[c]=r(c)}var o=document.createElement("script");o.type="text/javascript",o.async=!0,o.src=("https:"===document.location.protocol?"https:":"http:")+"//cdn.treasuredata.com/sdk/2.1/td.min.js";var a=document.getElementsByTagName("script")[0];a.parentNode.insertBefore(o,a)}}("Treasure",this);

  document.addEventListener('DOMContentLoaded', function (event) {
    function changeData(n,t){var i=n.replace(/\r?\n/g,"");return i=i.replace(/[^A-Za-z0-9]/g,""),i.slice(0,t)}var pagename=aaid=act_receipt_number=order_no="",url,aryURL,aryURL_order;try{_screq.pageName&&(pagename=_screq.pageName)}catch(e){}try{s.marketingCloudVisitorID&&(aaid=s.marketingCloudVisitorID)}catch(e){}url=location.href;aryURL=["https://info.nomura.co.jp/ctm/customer/open_account/entry","https://mn-account.nomura.co.jp/account/EntryComplete"];aryURL.forEach(function(n){if(url.indexOf(n)>=0){var t=document.getElementsByTagName("table");t.length>0&&Array.prototype.slice.call(t).forEach(function(n){var t=n.getElementsByTagName("tr"),i=Array.prototype.slice.call(t);i.forEach(function(n){var t=n.getElementsByTagName("th"),i=Array.prototype.slice.call(t);i.forEach(function(t){if(t.innerText==="受付番号"){var r=n.getElementsByTagName("td"),u=Array.prototype.slice.call(r),i=u[0].innerText;i=changeData(i,20);act_receipt_number=i}})})})}});aryURL_order=["https://mn-account.nomura.co.jp/account/MailaddressEntryConfirm"];aryURL_order.forEach(function(n){if(url.indexOf(n)>=0){var t=document.getElementById("order_no").textContent;t=changeData(t,9);order_no=t}})
    var td = new Treasure({
    host: 'tokyo.in.treasuredata.com',
    writeKey: '210/62001fccadb5bdeef9a716c2bcd8cfe18cd49c9e',
    database: 'ndmp',
    startInSignedMode: true
    });
    td.set('$global', 'td_global_id', 'td_global_id');
    td.set('$global', {c_pagename: pagename, c_aaid: aaid, c_act_receipt_number: act_receipt_number, c_appli_number: order_no});
    td.trackPageview('td_weblog');
  });
}());

/* Google Tag Manager 201911 */
(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','GTM-5WQGF67');
/* End Google Tag Manager */
