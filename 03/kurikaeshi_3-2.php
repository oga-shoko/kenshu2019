<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>繰り返し</title>
    <link rel="stylesheet" href="style.css">
  </head>
  <body>
    <form method="get" action="kurikaeshi_3-2.php">
        <input type="text" name="cols">
        行
        ×
        <input type="text" name="cols_2">
        列
        <input type="submit">
        <input type="reset">
    </form>

    <hr>

    <table border="1">
      <?php
      if(isset($_GET['cols'])){
        for($i=0; $i < $_GET['cols']; $i++){
          if(isset($_GET['cols_2'])){
            for($j=0; $j < $_GET['cols_2']; $j++){
              echo "<td>テスト</td>";
            }
          }
          echo "</tr>";
        }
      }
       ?>
     </table>
   </body>
</html>
