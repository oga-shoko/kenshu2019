<!DOCTYPE html>
<?php
  include("./functions.php");
  include("./statics.php");

  $pdo = initDB();

  if(isset($_GET['staff_id'])) { $param_id = $_GET['staff_id']; }
  $query_str = "SELECT
                m.staff_id,
                m.name,
                m.birthplace,
                m.gender,
                m.age,
                sm.section_name,
                gm.grade_name
                FROM member AS m
                LEFT JOIN section_master AS sm ON sm.section_id=m.section_id
                LEFT JOIN grade_master AS gm ON gm.grade_id=m.grade_id
                WHERE staff_id = '".$param_id."'";


  $query_str;
  $sql = $pdo->prepare($query_str);
  $sql->execute();
  $result = $sql->fetchAll();
  // SQLから返ってきたものを保存してる

 ?>

  <html>
   <head>
   <meta charset="utf-8">
     <meta name="viewport" content="width=device-width, initial-scale=1">
     <style>
      .button{
        text-align:right;
        font-size:0;
      }
      .button .submit{
        display: inline-block;
      }

      table{
        margin: auto;
      }

      table th{
        background-color: gray;
        color: white;
        width: 150px;
        height: auto;
      }
      table td{
        width: 150px;
        height: 50px;
        border-left:none;
        border-right: 2px solid gray;
        border-bottom: 2px solid gray;
      }
      .first{
        border-top: 2px solid gray;
      }

     </style>
     <script type="text/javascript">
     function skujyo(id){
       if(window.confirm('削除してよろしいですか？')){
         location.href = "./delete.php?staff_id=" + id;
        }
        else{
          window.alert('キャンセルされました');
          return false;
        }
      }
    </script>
     <link rel="stylesheet"href="test.css">
     <title>社員名簿システム</title>
   </head>
   <body>
     <?php include( dirname(__FILE__) . './header.php'); ?>
       <table>
       <tr>
         <th>社員ID</th>
         <td class="first"><?php echo $result[0]['staff_id'];?></td>
       </tr>
       <tr>
         <th>名前</th>
         <td><?php echo $result[0]['name'];?></td>
       </tr>
       <tr>
         <th>出身地</th>
         <td>
         <?php echo $prefecture_array[$result[0]['birthplace']];?></td>
       </tr>
       <tr>
         <th>性別</th>
         <td>
         <?php echo $gender_array[$result[0]['gender']];?></td>
       </tr>
       <tr>
         <th>年齢</th>
         <td><?php echo $result[0]['age'];?></td>
      </tr>
      <tr>
        <th>所属部署</th>
        <td><?php echo $result[0]['section_name'];?></td>
     </tr>
     <tr>
       <th>役職</th>
       <td><?php echo $result[0]['grade_name'];?></td>
    </tr>
         </table>
         <div class="button">
           <div class="submit">
             <form method="post" action="entry_update01.php">
               <input type="submit" value="編集">
               <input type="hidden" name="staff_id" value="<?php echo $param_id ?>">
             </form>
           </div>
           <div class="submit">
             <form method="post">
             <input type="button" value="削除"  onclick="skujyo(<?php echo $result[0]['staff_id']; ?>);">
             </form>
           </div>
         </div>

     </body>
    </html>
