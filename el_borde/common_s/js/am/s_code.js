/*!*
 ============== DO NOT ALTER ANYTHING BELOW THIS LINE ! ============

 Adobe Visitor API for JavaScript version: 1.8.0
 Copyright 1996-2015 Adobe, Inc. All Rights Reserved
 More info available at http://www.omniture.com
*/
function Visitor(t,e){if(!t){throw"Visitor requires Adobe Marketing Cloud Org ID"}var n=this;n.version="1.8.0";var i=window,a=i.Visitor;a.version=n.version,i.s_c_in||(i.s_c_il=[],i.s_c_in=0),n._c="Visitor",n._il=i.s_c_il,n._in=i.s_c_in,n._il[n._in]=n,i.s_c_in++,n.la={Ha:[]};var r=i.document,o=a.Db;o||(o=null);var s=a.Eb;s||(s=void 0);var c=a.Pa;c||(c=!0);var u=a.Na;u||(u=!1),n.ha=function(t){var e,n,i=0;if(t){for(e=0;e<t.length;e++){n=t.charCodeAt(e),i=(i<<5)-i+n,i&=i}}return i},n.r=function(t,e){var n,i,a="0123456789",r="",o="",s=8,u=10,l=10;if(e===d&&(D.isClientSideMarketingCloudVisitorID=c),1==t){for(a+="ABCDEF",n=0;16>n;n++){i=Math.floor(Math.random()*s),r+=a.substring(i,i+1),i=Math.floor(Math.random()*s),o+=a.substring(i,i+1),s=16}return r+"-"+o}for(n=0;19>n;n++){i=Math.floor(Math.random()*u),r+=a.substring(i,i+1),0==n&&9==i?u=3:(1==n||2==n)&&10!=u&&2>i?u=10:n>2&&(u=10),i=Math.floor(Math.random()*l),o+=a.substring(i,i+1),0==n&&9==i?l=3:(1==n||2==n)&&10!=l&&2>i?l=10:n>2&&(l=10)}return r+o},n.Ta=function(){var t;if(!t&&i.location&&(t=i.location.hostname),t){if(/^[0-9.]+$/.test(t)){t=""}else{var e=t.split("."),n=e.length-1,a=n-1;if(n>1&&2>=e[n].length&&(2==e[n-1].length||0>",ac,ad,ae,af,ag,ai,al,am,an,ao,aq,ar,as,at,au,aw,ax,az,ba,bb,be,bf,bg,bh,bi,bj,bm,bo,br,bs,bt,bv,bw,by,bz,ca,cc,cd,cf,cg,ch,ci,cl,cm,cn,co,cr,cu,cv,cw,cx,cz,de,dj,dk,dm,do,dz,ec,ee,eg,es,et,eu,fi,fm,fo,fr,ga,gb,gd,ge,gf,gg,gh,gi,gl,gm,gn,gp,gq,gr,gs,gt,gw,gy,hk,hm,hn,hr,ht,hu,id,ie,im,in,io,iq,ir,is,it,je,jo,jp,kg,ki,km,kn,kp,kr,ky,kz,la,lb,lc,li,lk,lr,ls,lt,lu,lv,ly,ma,mc,md,me,mg,mh,mk,ml,mn,mo,mp,mq,mr,ms,mt,mu,mv,mw,mx,my,na,nc,ne,nf,ng,nl,no,nr,nu,nz,om,pa,pe,pf,ph,pk,pl,pm,pn,pr,ps,pt,pw,py,qa,re,ro,rs,ru,rw,sa,sb,sc,sd,se,sg,sh,si,sj,sk,sl,sm,sn,so,sr,st,su,sv,sx,sy,sz,tc,td,tf,tg,th,tj,tk,tl,tm,tn,to,tp,tr,tt,tv,tw,tz,ua,ug,uk,us,uy,uz,va,vc,ve,vg,vi,vn,vu,wf,ws,yt,".indexOf(","+e[n]+","))&&a--,a>0){for(t="";n>=a;){t=e[n]+(t?".":"")+t,n--}}}}return t},n.cookieRead=function(t){var t=encodeURIComponent(t),e=(";"+r.cookie).split(" ").join(";"),n=e.indexOf(";"+t+"="),i=0>n?n:e.indexOf(";",n+1);return 0>n?"":decodeURIComponent(e.substring(n+2+t.length,0>i?e.length:i))},n.cookieWrite=function(t,e,i){var a,o=n.cookieLifetime,e=""+e,o=o?(""+o).toUpperCase():"";return i&&"SESSION"!=o&&"NONE"!=o?(a=""!=e?parseInt(o?o:0,10):-60)?(i=new Date,i.setTime(i.getTime()+1000*a)):1==i&&(i=new Date,a=i.getYear(),i.setYear(a+2+(1900>a?1900:0))):i=0,t&&"NONE"!=o?(r.cookie=encodeURIComponent(t)+"="+encodeURIComponent(e)+"; path=/;"+(i?" expires="+i.toGMTString()+";":"")+(n.cookieDomain?" domain="+n.cookieDomain+";":""),n.cookieRead(t)==e):0},n.h=o,n.L=function(t,e){try{"function"==typeof t?t.apply(i,e):t[1].apply(t[0],e)}catch(n){}},n.Ya=function(t,e){e&&(n.h==o&&(n.h={}),n.h[t]==s&&(n.h[t]=[]),n.h[t].push(e))},n.q=function(t,e){if(n.h!=o){var i=n.h[t];if(i){for(;0<i.length;){n.L(i.shift(),e)}}}},n.w=function(t,e,n,i){if(n=encodeURIComponent(e)+"="+encodeURIComponent(n),e=w.wb(t),t=w.ob(t),-1===t.indexOf("?")){return t+"?"+n+e}var a=t.split("?"),t=a[0]+"?",i=w.ab(a[1],n,i);return t+i+e},n.Sa=function(t,e){var n=RegExp("[\\?&#]"+e+"=([^&#]*)").exec(t);return n&&n.length?decodeURIComponent(n[1]):void 0},n.Xa=function(){var t=o,e=i.location.href;try{var a=n.Sa(e,C.ba);if(a){for(var t={},r=a.split("|"),e=0,s=r.length;s>e;e++){var c=r[e].split("=");t[c[0]]=decodeURIComponent(c[1])}}return t}catch(u){}},n.Qa=function(){var t=n.Xa();if(t){var e=t[d],i=n.setMarketingCloudVisitorID;e&&e.match(C.v)&&i(e),n.j(k,-1),t=t[y],e=n.setAnalyticsVisitorID,t&&t.match(C.v)&&e(t)}},n.l=o,n.Va=function(t,e,i,a){e=n.w(e,"d_fieldgroup",t,1),a.url=n.w(a.url,"d_fieldgroup",t,1),a.m=n.w(a.m,"d_fieldgroup",t,1),D.c[t]=c,a===Object(a)&&a.m&&"XMLHttpRequest"===n.na.F.G?n.na.kb(a,i,t):n.useCORSOnly||n.ka(t,e,i)},n.ka=function(t,e,i){var a,s=0,u=0;if(e&&r){for(a=0;!s&&2>a;){try{s=(s=r.getElementsByTagName(a>0?"HEAD":"head"))&&0<s.length?s[0]:0}catch(l){s=0}a++}if(!s){try{r.body&&(s=r.body)}catch(d){s=0}}if(s){for(a=0;!u&&2>a;){try{u=r.createElement(a>0?"SCRIPT":"script")}catch(f){u=0}a++}}}e&&s&&u?(u.type="text/javascript",u.src=e,s.firstChild?s.insertBefore(u,s.firstChild):s.appendChild(u),s=n.loadTimeout,P.c[t]={requestStart:P.o(),url:e,wa:s,ua:P.Aa(),va:0},i&&(n.l==o&&(n.l={}),n.l[t]=setTimeout(function(){i(c)},s)),n.la.Ha.push(e)):i&&i()},n.Ra=function(t){n.l!=o&&n.l[t]&&(clearTimeout(n.l[t]),n.l[t]=0)},n.ia=u,n.ja=u,n.isAllowed=function(){return !n.ia&&(n.ia=c,n.cookieRead(n.cookieName)||n.cookieWrite(n.cookieName,"T",1))&&(n.ja=c),n.ja},n.b=o,n.e=o;var l=a.Vb;l||(l="MC");var d=a.ac;d||(d="MCMID");var f=a.Wb;f||(f="MCCIDH");var h=a.Zb;h||(h="MCSYNCS");var g=a.$b;g||(g="MCSYNCSOP");var p=a.Xb;p||(p="MCIDTS");var v=a.Yb;v||(v="MCOPTOUT");var m=a.Tb;m||(m="A");var y=a.Qb;y||(y="MCAID");var b=a.Ub;b||(b="AAM");var S=a.Sb;S||(S="MCAAMLH");var k=a.Rb;k||(k="MCAAMB");var E=a.bc;E||(E="NONE"),n.N=0,n.ga=function(){if(!n.N){var t=n.version;n.audienceManagerServer&&(t+="|"+n.audienceManagerServer),n.audienceManagerServerSecure&&(t+="|"+n.audienceManagerServerSecure),n.N=n.ha(t)}return n.N},n.ma=u,n.f=function(){if(!n.ma){n.ma=c;var t,e,i,a,r=n.ga(),s=u,l=n.cookieRead(n.cookieName),d=new Date;if(n.b==o&&(n.b={}),l&&"T"!=l){for(l=l.split("|"),l[0].match(/^[\-0-9]+$/)&&(parseInt(l[0],10)!=r&&(s=c),l.shift()),1==l.length%2&&l.pop(),r=0;r<l.length;r+=2){t=l[r].split("-"),e=t[0],i=l[r+1],1<t.length?(a=parseInt(t[1],10),t=0<t[1].indexOf("s")):(a=0,t=u),s&&(e==f&&(i=""),a>0&&(a=d.getTime()/1000-60)),e&&i&&(n.d(e,i,1),a>0&&(n.b["expire"+e]=a+(t?"s":""),d.getTime()>=1000*a||t&&!n.cookieRead(n.sessionCookieName)))&&(n.e||(n.e={}),n.e[e]=c)}}!n.a(y)&&(l=n.cookieRead("s_vi"))&&(l=l.split("|"),1<l.length&&0<=l[0].indexOf("v1")&&(i=l[1],r=i.indexOf("["),r>=0&&(i=i.substring(0,r)),i&&i.match(C.v)&&n.d(y,i)))}},n.$a=function(){var t,e,i=n.ga();for(t in n.b){!Object.prototype[t]&&n.b[t]&&"expire"!=t.substring(0,6)&&(e=n.b[t],i+=(i?"|":"")+t+(n.b["expire"+t]?"-"+n.b["expire"+t]:"")+"|"+e)}n.cookieWrite(n.cookieName,i,1)},n.a=function(t,e){return n.b==o||!e&&n.e&&n.e[t]?o:n.b[t]},n.d=function(t,e,i){n.b==o&&(n.b={}),n.b[t]=e,i||n.$a()},n.Ua=function(t,e){var i=n.a(t,e);return i?i.split("*"):o},n.Za=function(t,e,i){n.d(t,e?e.join("*"):"",i)},n.Kb=function(t,e){var i=n.Ua(t,e);if(i){var a,r={};for(a=0;a<i.length;a+=2){r[i[a]]=i[a+1]}return r}return o},n.Mb=function(t,e,i){var a,r=o;if(e){for(a in r=[],e){Object.prototype[a]||(r.push(a),r.push(e[a]))}}n.Za(t,r,i)},n.j=function(t,e,i){var a=new Date;a.setTime(a.getTime()+1000*e),n.b==o&&(n.b={}),n.b["expire"+t]=Math.floor(a.getTime()/1000)+(i?"s":""),0>e?(n.e||(n.e={}),n.e[t]=c):n.e&&(n.e[t]=u),i&&(n.cookieRead(n.sessionCookieName)||n.cookieWrite(n.sessionCookieName,"1"))},n.fa=function(t){return t&&("object"==typeof t&&(t=t.d_mid?t.d_mid:t.visitorID?t.visitorID:t.id?t.id:t.uuid?t.uuid:""+t),t&&(t=t.toUpperCase(),"NOTARGET"==t&&(t=E)),!t||t!=E&&!t.match(C.v))&&(t=""),t},n.k=function(t,e){if(n.Ra(t),n.i!=o&&(n.i[t]=u),P.c[t]&&(P.c[t].Bb=P.o(),P.K(t)),D.c[t]&&D.Ja(t,u),t==l){D.isClientSideMarketingCloudVisitorID!==c&&(D.isClientSideMarketingCloudVisitorID=u);var i=n.a(d);if(!i){if(i="object"==typeof e&&e.mid?e.mid:n.fa(e),!i){if(n.D){return void n.getAnalyticsVisitorID(o,u,c)}i=n.r(0,d)}n.d(d,i)}i&&i!=E||(i=""),"object"==typeof e&&((e.d_region||e.dcs_region||e.d_blob||e.blob)&&n.k(b,e),n.D&&e.mid&&n.k(m,{id:e.id})),n.q(d,[i])}if(t==b&&"object"==typeof e){i=604800,e.id_sync_ttl!=s&&e.id_sync_ttl&&(i=parseInt(e.id_sync_ttl,10));var a=n.a(S);a||((a=e.d_region)||(a=e.dcs_region),a&&(n.j(S,i),n.d(S,a))),a||(a=""),n.q(S,[a]),a=n.a(k),(e.d_blob||e.blob)&&((a=e.d_blob)||(a=e.blob),n.j(k,i),n.d(k,a)),a||(a=""),n.q(k,[a]),!e.error_msg&&n.B&&n.d(f,n.B)}if(t==m&&(i=n.a(y),i||((i=n.fa(e))?i!==E&&n.j(k,-1):i=E,n.d(y,i)),i&&i!=E||(i=""),n.q(y,[i])),n.idSyncDisableSyncs?I.Ba=c:(I.Ba=u,i={},i.ibs=e.ibs,i.subdomain=e.subdomain,I.xb(i)),e===Object(e)){var r;n.isAllowed()&&(r=n.a(v)),r||(r=E,e.d_optout&&e.d_optout instanceof Array&&(r=e.d_optout.join(",")),i=parseInt(e.d_ottl,10),isNaN(i)&&(i=7200),n.j(v,i,c),n.d(v,r)),n.q(v,[r])}},n.i=o,n.s=function(t,e,i,a,r){var s,u="",f=w.qb(t);return !n.isAllowed()||(n.f(),u=n.a(t),n.disableThirdPartyCalls&&!u&&(t===d?(u=n.r(0,d),n.setMarketingCloudVisitorID(u)):t===y&&!f&&(u="",n.setAnalyticsVisitorID(u))),u||n.disableThirdPartyCalls&&!f)||(t==d||t==v?s=l:t==S||t==k?s=b:t==y&&(s=m),!s)?(t!=d&&t!=y||u!=E||(u="",a=c),i&&a&&n.L(i,[u]),u):(!e||n.i!=o&&n.i[s]||(n.i==o&&(n.i={}),n.i[s]=c,n.Va(s,e,function(e,i){if(!n.a(t)){if(P.c[s]&&(P.c[s].timeout=P.o(),P.c[s].pb=!!e,P.K(s)),i!==Object(i)||n.useCORSOnly){e&&D.Ja(s,c);var a="";t==d?a=n.r(0,d):s==b&&(a={error_msg:"timeout"}),n.k(s,a)}else{n.ka(s,i.url,i.I)}}},r)),n.Ya(t,i),e||n.k(s,{id:E}),"")},n._setMarketingCloudFields=function(t){n.f(),n.k(l,t)},n.setMarketingCloudVisitorID=function(t){n._setMarketingCloudFields(t)},n.D=u,n.getMarketingCloudVisitorID=function(t,e){if(n.isAllowed()){n.marketingCloudServer&&0>n.marketingCloudServer.indexOf(".demdex.net")&&(n.D=c);var i=n.A("_setMarketingCloudFields");return n.s(d,i.url,t,e,i)}return""},n.Wa=function(){n.getAudienceManagerBlob()},a.AuthState={UNKNOWN:0,AUTHENTICATED:1,LOGGED_OUT:2},n.z={},n.ea=u,n.B="",n.setCustomerIDs=function(t){if(n.isAllowed()&&t){n.f();var e,i;for(e in t){if(!Object.prototype[e]&&(i=t[e])){if("object"==typeof i){var a={};i.id&&(a.id=i.id),i.authState!=s&&(a.authState=i.authState),n.z[e]=a}else{n.z[e]={id:i}}}}var t=n.getCustomerIDs(),a=n.a(f),r="";a||(a=0);for(e in t){Object.prototype[e]||(i=t[e],r+=(r?"|":"")+e+"|"+(i.id?i.id:"")+(i.authState?i.authState:""))}n.B=n.ha(r),n.B!=a&&(n.ea=c,n.Wa())}},n.getCustomerIDs=function(){n.f();var t,e,i={};for(t in n.z){Object.prototype[t]||(e=n.z[t],i[t]||(i[t]={}),e.id&&(i[t].id=e.id),i[t].authState=e.authState!=s?e.authState:a.AuthState.UNKNOWN)}return i},n._setAnalyticsFields=function(t){n.f(),n.k(m,t)},n.setAnalyticsVisitorID=function(t){n._setAnalyticsFields(t)},n.getAnalyticsVisitorID=function(t,e,i){if(n.isAllowed()){var a="";if(i||(a=n.getMarketingCloudVisitorID(function(){n.getAnalyticsVisitorID(t,c)})),a||i){var r=i?n.marketingCloudServer:n.trackingServer,o="";n.loadSSL&&(i?n.marketingCloudServerSecure&&(r=n.marketingCloudServerSecure):n.trackingServerSecure&&(r=n.trackingServerSecure));var s={};if(r){var r="http"+(n.loadSSL?"s":"")+"://"+r+"/id",a="d_visid_ver="+n.version+"&mcorgid="+encodeURIComponent(n.marketingCloudOrgID)+(a?"&mid="+encodeURIComponent(a):"")+(n.idSyncDisable3rdPartySyncing?"&d_coppa=true":""),u=["s_c_il",n._in,"_set"+(i?"MarketingCloud":"Analytics")+"Fields"],o=r+"?"+a+"&callback=s_c_il%5B"+n._in+"%5D._set"+(i?"MarketingCloud":"Analytics")+"Fields";s.m=r+"?"+a,s.ra=u}return s.url=o,n.s(i?d:y,o,t,e,s)}}return""},n._setAudienceManagerFields=function(t){n.f(),n.k(b,t)},n.A=function(t){var e=n.audienceManagerServer,i="",a=n.a(d),r=n.a(k,c),o=n.a(y),o=o&&o!=E?"&d_cid_ic=AVID%01"+encodeURIComponent(o):"";if(n.loadSSL&&n.audienceManagerServerSecure&&(e=n.audienceManagerServerSecure),e){var s,u,i=n.getCustomerIDs();if(i){for(s in i){Object.prototype[s]||(u=i[s],o+="&d_cid_ic="+encodeURIComponent(s)+"%01"+encodeURIComponent(u.id?u.id:"")+(u.authState?"%01"+u.authState:""))}}return t||(t="_setAudienceManagerFields"),e="http"+(n.loadSSL?"s":"")+"://"+e+"/id",a="d_visid_ver="+n.version+"&d_rtbd=json&d_ver=2"+(!a&&n.D?"&d_verify=1":"")+"&d_orgid="+encodeURIComponent(n.marketingCloudOrgID)+"&d_nsid="+(n.idSyncContainerID||0)+(a?"&d_mid="+encodeURIComponent(a):"")+(n.idSyncDisable3rdPartySyncing?"&d_coppa=true":"")+(r?"&d_blob="+encodeURIComponent(r):"")+o,r=["s_c_il",n._in,t],i=e+"?"+a+"&d_cb=s_c_il%5B"+n._in+"%5D."+t,{url:i,m:e+"?"+a,ra:r}}return{url:i}},n.getAudienceManagerLocationHint=function(t,e){if(n.isAllowed()&&n.getMarketingCloudVisitorID(function(){n.getAudienceManagerLocationHint(t,c)})){var i=n.a(y);if(i||(i=n.getAnalyticsVisitorID(function(){n.getAudienceManagerLocationHint(t,c)})),i){return i=n.A(),n.s(S,i.url,t,e,i)}}return""},n.getAudienceManagerBlob=function(t,e){if(n.isAllowed()&&n.getMarketingCloudVisitorID(function(){n.getAudienceManagerBlob(t,c)})){var i=n.a(y);if(i||(i=n.getAnalyticsVisitorID(function(){n.getAudienceManagerBlob(t,c)})),i){var i=n.A(),a=i.url;return n.ea&&n.j(k,-1),n.s(k,a,t,e,i)}}return""},n.t="",n.C={},n.O="",n.P={},n.getSupplementalDataID=function(t,e){!n.t&&!e&&(n.t=n.r(1));var i=n.t;return n.O&&!n.P[t]?(i=n.O,n.P[t]=c):i&&(n.C[t]&&(n.O=n.t,n.P=n.C,n.t=i=e?"":n.r(1),n.C={}),i&&(n.C[t]=c)),i},a.OptOut={GLOBAL:"global"},n.getOptOut=function(t,e){if(n.isAllowed()){var i=n.A("_setMarketingCloudFields");return n.s(v,i.url,t,e,i)}return""},n.isOptedOut=function(t,e,i){return n.isAllowed()?(e||(e=a.OptOut.GLOBAL),(i=n.getOptOut(function(i){n.L(t,[i==a.OptOut.GLOBAL||0<=i.indexOf(e)])},i))?i==a.OptOut.GLOBAL||0<=i.indexOf(e):o):u},n.appendVisitorIDsTo=function(t){for(var e=C.ba,i=[[d,n.a(d)],[y,n.a(y)]],a="",r=0,s=i.length;s>r;r++){var c=i[r],u=c[0],c=c[1];c!=o&&c!==E&&(a=a?a+="|":a,a+=u+"="+encodeURIComponent(c))}try{return n.w(t,e,a)}catch(l){return t}};var C={p:!!i.postMessage,Ma:1,da:86400000,ba:"adobe_mc",v:/^[0-9a-fA-F\-]+$/};n.Fb=C,n.pa={postMessage:function(t,e,n){var i=1;e&&(C.p?n.postMessage(t,e.replace(/([^:]+:\/\/[^\/]+).*/,"$1")):e&&(n.location=e.replace(/#.*$/,"")+"#"+ +new Date+i+++"&"+t))},X:function(t,e){var n;try{C.p&&(t&&(n=function(n){return"string"==typeof e&&n.origin!==e||"[object Function]"===Object.prototype.toString.call(e)&&!1===e(n.origin)?!1:void t(n)}),window.addEventListener?window[t?"addEventListener":"removeEventListener"]("message",n,!1):window[t?"attachEvent":"detachEvent"]("onmessage",n))}catch(i){}}};var w={Q:function(){return r.addEventListener?function(t,e,n){t.addEventListener(e,function(t){"function"==typeof n&&n(t)},u)}:r.attachEvent?function(t,e,n){t.attachEvent("on"+e,function(t){"function"==typeof n&&n(t)})}:void 0}(),map:function(t,e){if(Array.prototype.map){return t.map(e)}if(void 0===t||t===o){throw new TypeError}var n=Object(t),i=n.length>>>0;if("function"!=typeof e){throw new TypeError}for(var a=Array(i),r=0;i>r;r++){r in n&&(a[r]=e.call(e,n[r],r,n))}return a},jb:function(t,e){return this.map(t,function(t){return encodeURIComponent(t)}).join(e)},wb:function(t){var e=t.indexOf("#");return e>0?t.substr(e):""},ob:function(t){var e=t.indexOf("#");return e>0?t.substr(0,e):t},ab:function(t,e,n){return t=t.split("&"),n=n!=o?n:t.length,t.splice(n,0,e),t.join("&")},qb:function(t,e,i){return t!==y?u:(e||(e=n.trackingServer),i||(i=n.trackingServerSecure),t=n.loadSSL?i:e,"string"==typeof t&&t.length?0>t.indexOf("2o7.net")&&0>t.indexOf("omtrdc.net"):u)}};n.Lb=w;var T={F:function(){var t="none",e=c;return"undefined"!=typeof XMLHttpRequest&&XMLHttpRequest===Object(XMLHttpRequest)&&("withCredentials" in new XMLHttpRequest?t="XMLHttpRequest":new Function("/*@cc_on return /^10/.test(@_jscript_version) @*/")()?t="XMLHttpRequest":"undefined"!=typeof XDomainRequest&&XDomainRequest===Object(XDomainRequest)&&(e=u),0<Object.prototype.toString.call(window.Cb).indexOf("Constructor")&&(e=u)),{G:t,Ob:e}}(),lb:function(){return"none"===this.F.G?o:new window[this.F.G]},kb:function(t,e,i){var a=this;e&&(t.I=e);try{var r=this.lb();r.open("get",t.m+"&ts="+(new Date).getTime(),c),"XMLHttpRequest"===this.F.G&&(r.withCredentials=c,r.timeout=n.loadTimeout,r.setRequestHeader("Content-Type","application/x-www-form-urlencoded"),r.onreadystatechange=function(){if(4===this.readyState&&200===this.status){t:{var e;try{if(e=JSON.parse(this.responseText),e!==Object(e)){a.n(t,o,"Response is not JSON");break t}}catch(n){a.n(t,n,"Error parsing response as JSON");break t}try{for(var i=t.ra,r=window,s=0;s<i.length;s++){r=r[i[s]]}r(e)}catch(c){a.n(t,c,"Error forming callback function")}}}}),r.onerror=function(e){a.n(t,e,"onerror")},r.ontimeout=function(e){a.n(t,e,"ontimeout")},r.send(),P.c[i]={requestStart:P.o(),url:t.m,wa:r.timeout,ua:P.Aa(),va:1},n.la.Ha.push(t.m)}catch(s){this.n(t,s,"try-catch")}},n:function(t,e,i){n.CORSErrors.push({Pb:t,error:e,description:i}),t.I&&("ontimeout"===i?t.I(c):t.I(u,t))}};n.na=T;var I={Oa:30000,ca:649,La:u,id:o,W:[],T:o,za:function(t){return"string"==typeof t?(t=t.split("/"),t[0]+"//"+t[2]):void 0},g:o,url:o,mb:function(){var t="http://fast.",e="?d_nsid="+n.idSyncContainerID+"#"+encodeURIComponent(r.location.href);return this.g||(this.g="nosubdomainreturned"),n.loadSSL&&(t=n.idSyncSSLUseAkamai?"https://fast.":"https://"),t=t+this.g+".demdex.net/dest5.html"+e,this.T=this.za(t),this.id="destination_publishing_iframe_"+this.g+"_"+n.idSyncContainerID,t},eb:function(){var t="?d_nsid="+n.idSyncContainerID+"#"+encodeURIComponent(r.location.href);"string"==typeof n.M&&n.M.length&&(this.id="destination_publishing_iframe_"+(new Date).getTime()+"_"+n.idSyncContainerID,this.T=this.za(n.M),this.url=n.M+t)},Ba:o,xa:u,Z:u,H:o,cc:o,vb:o,dc:o,Y:u,J:[],tb:[],ub:[],Da:C.p?15:100,U:[],rb:[],sa:c,Ga:u,Fa:function(){return !n.idSyncDisable3rdPartySyncing&&(this.xa||n.Hb)&&this.g&&"nosubdomainreturned"!==this.g&&this.url&&!this.Z},R:function(){function t(){i=document.createElement("iframe"),i.sandbox="allow-scripts allow-same-origin",i.title="Adobe ID Syncing iFrame",i.id=n.id,i.style.cssText="display: none; width: 0; height: 0;",i.src=n.url,n.vb=c,e(),document.body.appendChild(i)}function e(){w.Q(i,"load",function(){i.className="aamIframeLoaded",n.H=c,n.u()})}this.Z=c;var n=this,i=document.getElementById(this.id);i?"IFRAME"!==i.nodeName?(this.id+="_2",t()):"aamIframeLoaded"!==i.className?e():(this.H=c,this.Ca=i,this.u()):t(),this.Ca=i},u:function(t){var e=this;t===Object(t)&&(this.U.push(t),this.yb(t)),(this.Ga||!C.p||this.H)&&this.U.length&&(this.K(this.U.shift()),this.u()),!n.idSyncDisableSyncs&&this.H&&this.J.length&&!this.Y&&(this.La||(this.La=c,setTimeout(function(){e.Da=C.p?15:150},this.Oa)),this.Y=c,this.Ia())},yb:function(t){var e,n,i;if((e=t.ibs)&&e instanceof Array&&(n=e.length)){for(t=0;n>t;t++){i=e[t],i.syncOnPage&&this.ta(i,"","syncOnPage")}}},K:function(t){var e,n,i,a,r,o=encodeURIComponent;if((e=t.ibs)&&e instanceof Array&&(n=e.length)){for(i=0;n>i;i++){a=e[i],r=[o("ibs"),o(a.id||""),o(a.tag||""),w.jb(a.url||[],","),o(a.ttl||""),"","",a.fireURLSync?"true":"false"],a.syncOnPage||(this.sa?this.qa(r.join("|")):a.fireURLSync&&this.ta(a,r.join("|")))}}this.rb.push(t)},ta:function(t,e,i){var a=(i="syncOnPage"===i?c:u)?g:h;n.f();var r=n.a(a),o=u,s=u,l=Math.ceil((new Date).getTime()/C.da);r?(r=r.split("*"),s=this.zb(r,t.id,l),o=s.hb,s=s.ib,(!o||!s)&&this.ya(i,t,e,r,a,l)):(r=[],this.ya(i,t,e,r,a,l))},zb:function(t,e,n){var i,a,r,o=u,s=u;for(a=0;a<t.length;a++){i=t[a],r=parseInt(i.split("-")[1],10),i.match("^"+e+"-")?(o=c,r>n?s=c:(t.splice(a,1),a--)):n>=r&&(t.splice(a,1),a--)}return{hb:o,ib:s}},sb:function(t){if(t.join("*").length>this.ca){for(t.sort(function(t,e){return parseInt(t.split("-")[1],10)-parseInt(e.split("-")[1],10)});t.join("*").length>this.ca;){t.shift()}}},ya:function(t,e,i,a,r,s){var c=this;if(t){if("img"===e.tag){var u,l,d,t=e.url,i=n.loadSSL?"https:":"http:";for(a=0,u=t.length;u>a;a++){l=t[a],d=/^\/\//.test(l);var f=new Image;w.Q(f,"load",function(t,e,i,a){return function(){c.W[t]=o,n.f();var s=n.a(r),u=[];if(s){var l,d,f,s=s.split("*");for(l=0,d=s.length;d>l;l++){f=s[l],f.match("^"+e.id+"-")||u.push(f)}}c.Ka(u,e,i,a)}}(this.W.length,e,r,s)),f.src=(d?i:"")+l,this.W.push(f)}}}else{this.qa(i),this.Ka(a,e,r,s)}},qa:function(t){var e=encodeURIComponent;this.J.push(e(n.Ib?"---destpub-debug---":"---destpub---")+t)},Ka:function(t,e,i,a){t.push(e.id+"-"+(a+Math.ceil(e.ttl/60/24))),this.sb(t),n.d(i,t.join("*"))},Ia:function(){var t,e=this;this.J.length?(t=this.J.shift(),n.pa.postMessage(t,this.url,this.Ca.contentWindow),this.tb.push(t),setTimeout(function(){e.Ia()},this.Da)):this.Y=u},X:function(t){var e=/^---destpub-to-parent---/;"string"==typeof t&&e.test(t)&&(e=t.replace(e,"").split("|"),"canSetThirdPartyCookies"===e[0]&&(this.sa="true"===e[1]?c:u,this.Ga=c,this.u()),this.ub.push(t))},xb:function(t){(this.url===o||t.subdomain&&"nosubdomainreturned"===this.g)&&(this.g="string"==typeof n.oa&&n.oa.length?n.oa:t.subdomain||"",this.url=this.mb()),t.ibs instanceof Array&&t.ibs.length&&(this.xa=c),this.Fa()&&(n.idSyncAttachIframeOnWindowLoad?(a.aa||"complete"===r.readyState||"loaded"===r.readyState)&&this.R():this.bb()),"function"==typeof n.idSyncIDCallResult?n.idSyncIDCallResult(t):this.u(t),"function"==typeof n.idSyncAfterIDCallResult&&n.idSyncAfterIDCallResult(t)},cb:function(t,e){return n.Jb||!t||e-t>C.Ma},bb:function(){function t(){e.Z||(document.body?e.R():setTimeout(t,30))}var e=this;t()}};n.Gb=I,n.timeoutMetricsLog=[];var P={gb:window.performance&&window.performance.timing?1:0,Ea:window.performance&&window.performance.timing?window.performance.timing:o,$:o,S:o,c:{},V:[],send:function(t){if(n.takeTimeoutMetrics&&t===Object(t)){var e,i=[],a=encodeURIComponent;for(e in t){t.hasOwnProperty(e)&&i.push(a(e)+"="+a(t[e]))}t="http"+(n.loadSSL?"s":"")+"://dpm.demdex.net/event?d_visid_ver="+n.version+"&d_visid_stg_timeout="+n.loadTimeout+"&"+i.join("&")+"&d_orgid="+a(n.marketingCloudOrgID)+"&d_timingapi="+this.gb+"&d_winload="+this.nb()+"&d_ld="+this.o(),(new Image).src=t,n.timeoutMetricsLog.push(t)}},nb:function(){return this.S===o&&(this.S=this.Ea?this.$-this.Ea.navigationStart:this.$-a.fb),this.S},o:function(){return(new Date).getTime()},K:function(t){var e=this.c[t],n={};n.d_visid_stg_timeout_captured=e.wa,n.d_visid_cors=e.va,n.d_fieldgroup=t,n.d_settimeout_overriden=e.ua,e.timeout?e.pb?(n.d_visid_timedout=1,n.d_visid_timeout=e.timeout-e.requestStart,n.d_visid_response=-1):(n.d_visid_timedout="n/a",n.d_visid_timeout="n/a",n.d_visid_response="n/a"):(n.d_visid_timedout=0,n.d_visid_timeout=-1,n.d_visid_response=e.Bb-e.requestStart),n.d_visid_url=e.url,a.aa?this.send(n):this.V.push(n),delete this.c[t]},Ab:function(){for(var t=0,e=this.V.length;e>t;t++){this.send(this.V[t])}},Aa:function(){return"function"==typeof setTimeout.toString?-1<setTimeout.toString().indexOf("[native code]")?0:1:-1}};n.Nb=P;var D={isClientSideMarketingCloudVisitorID:o,MCIDCallTimedOut:o,AnalyticsIDCallTimedOut:o,AAMIDCallTimedOut:o,c:{},Ja:function(t,e){switch(t){case l:e===u?this.MCIDCallTimedOut!==c&&(this.MCIDCallTimedOut=u):this.MCIDCallTimedOut=e;break;case m:e===u?this.AnalyticsIDCallTimedOut!==c&&(this.AnalyticsIDCallTimedOut=u):this.AnalyticsIDCallTimedOut=e;break;case b:e===u?this.AAMIDCallTimedOut!==c&&(this.AAMIDCallTimedOut=u):this.AAMIDCallTimedOut=e}}};if(n.isClientSideMarketingCloudVisitorID=function(){return D.isClientSideMarketingCloudVisitorID},n.MCIDCallTimedOut=function(){return D.MCIDCallTimedOut},n.AnalyticsIDCallTimedOut=function(){return D.AnalyticsIDCallTimedOut},n.AAMIDCallTimedOut=function(){return D.AAMIDCallTimedOut},n.idSyncGetOnPageSyncInfo=function(){return n.f(),n.a(g)},0>t.indexOf("@")&&(t+="@AdobeOrg"),n.marketingCloudOrgID=t,n.cookieName="AMCV_"+t,n.sessionCookieName="AMCVS_"+t,n.cookieDomain=n.Ta(),n.cookieDomain==i.location.hostname&&(n.cookieDomain=""),n.loadSSL=0<=i.location.protocol.toLowerCase().indexOf("https"),n.loadTimeout=30000,n.CORSErrors=[],n.marketingCloudServer=n.audienceManagerServer="dpm.demdex.net",n.Qa(),e&&"object"==typeof e){for(var A in e){!Object.prototype[A]&&(n[A]=e[A])}n.idSyncContainerID=n.idSyncContainerID||0,n.f(),T=n.a(p),A=Math.ceil((new Date).getTime()/C.da),!n.idSyncDisableSyncs&&I.cb(T,A)&&(n.j(k,-1),n.d(p,A)),n.getMarketingCloudVisitorID(),n.getAudienceManagerLocationHint(),n.getAudienceManagerBlob()}if(!n.idSyncDisableSyncs){I.eb(),w.Q(window,"load",function(){a.aa=c,P.$=P.o(),P.Ab();var t=I;t.Fa()&&t.R()});try{n.pa.X(function(t){I.X(t.data)},I.T)}catch(L){}}}Visitor.getInstance=function(t,e){var n,i,a=window.s_c_il;if(0>t.indexOf("@")&&(t+="@AdobeOrg"),a){for(i=0;i<a.length;i++){if((n=a[i])&&"Visitor"==n._c&&n.marketingCloudOrgID==t){return n}}}return new Visitor(t,e)},function(){function t(){e.aa=n}var e=window.Visitor,n=e.Pa,i=e.Na;n||(n=!0),i||(i=!1),window.addEventListener?window.addEventListener("load",t):window.attachEvent&&window.attachEvent("onload",t),e.fb=(new Date).getTime()}(),Visitor.getInstance=function(q,v){var y,a=window.s_c_il,m;0>q.indexOf("@")&&(q+="@AdobeOrg");if(a){for(m=0;m<a.length;m++){if((y=a[m])&&"Visitor"==y._c&&y.marketingCloudOrgID==q){return y}}}return new Visitor(q,v)};(function(){function q(){v.Y=y}var v=window.Visitor,y=v.Oa,a=v.Ma;y||(y=!0);a||(a=!1);window.addEventListener?window.addEventListener("load",q):window.attachEvent&&window.attachEvent("onload",q);v.eb=(new Date).getTime()})();visitor=new Visitor("2524217B5809E2D70A495C57@AdobeOrg");crmIdVal=(function(){var st="";var ed="";var c_name="crm_id";if(document.cookie.length>0){st=document.cookie.indexOf(c_name+"=");if(st!=-1){st=st+c_name.length+1;ed=document.cookie.indexOf(";",st);if(ed==-1){ed=document.cookie.length}return unescape(document.cookie.substring(st,ed))}}return""})();if(crmIdVal!==""&&crmIdVal!=="TRUE"){switch(document.domain){case"ysec-trading-x.uat.gt-starmf.jp":case"xsec-trading-x.uat.gt-starmf.jp":case"zsec-trading-x.uat.gt-starmf.jp":case"ysec-trading-y.uat.gt-starmf.jp":case"xsec-trading-y.uat.gt-starmf.jp":case"zsec-trading-y.uat.gt-starmf.jp":case"st-xsec-trading.gt-starmf.jp":case"st-zsec-trading.gt-starmf.jp":case"xsec-trading.gt-starmf.jp":case"zsec-trading.gt-starmf.jp":visitor.setCustomerIDs({dev_crm_id:{id:crmIdVal,authState:Visitor.AuthState.AUTHENTICATED}});break;case"hometrade.nomura.co.jp":case"st-ysec-trading.gt-starmf.jp":visitor.setCustomerIDs({crm_id:{id:crmIdVal,authState:Visitor.AuthState.AUTHENTICATED}});break}(function(){return function(c_name,value,domain){var path="/";var s=c_name+"="+escape(value)+"; path="+path+"; domain="+domain;document.cookie=s}})()("crm_id","TRUE",document.domain); }

var _sc = _sc || {};
var s_account = "nomurasecmain";
if (!window._screq) {
    window._screq = {}
}
window._screq.editversion = "20171002_18_30";
_screq.linkInternalFilters = "javascript:,nomura.co.jp,nomura.jp,qhit.net," + location.host;
_screq._linkInternalFiltersArr = _screq.linkInternalFilters.split(",");
_screq.remove_becon = [];
var s = s_gi(s_account);
if (document.cookie.match("sc_debug=T")) {
    s_account = "nomurasecdev";
    s = s_gi(s_account);
    _screq.debugmode = "T"
}
s.visitor = Visitor.getInstance("2524217B5809E2D70A495C57@AdobeOrg");
s.trancefer_tmp = function() {
    for (var i in window._screq) {
        if (window._screq != "object" && window._screq != "function") {
            s[i] = window._screq[i]
        }
    }
};
s.trancefer_tmp();
s.s_code_version = s.version + "_nomura_2014_" + window._screq.editversion + "";
s.charSet = "UTF-8";
s.linkInternalFilters = _screq.linkInternalFilters;
s.fpCookieDomainPeriods = location.host.match(/\.(co|ne|or)\.jp$/) ? "3" : "2";
s.cookieDomainPeriods = "3";
s.trackingServer = "metric-nonssl.nomura.co.jp";
s.trackingServerSecure = "metric.nomura.co.jp";
s.visitorNamespace = "nomurasec";
/*!************************* CONFIG SECTION **************************/
s.currencyCode = "JPY";
s.trackDownloadLinks = true;
s.trackExternalLinks = false;
s.trackInlineStats = true;
s.linkDownloadFileTypes = "exe,zip,wav,mp3,mov,mpg,avi,wmv,pdf,doc,docx,xls,xlsx,ppt,pptx";
s.linkLeaveQueryString = true;
s.linkTrackVars = "None";
s.linkTrackEvents = "None";
/*!************************* DEFVAR SECTION **************************/
_sc._defVarHashBefore = _sc._defVarHash;
_sc._defEventHashBefore = _sc._defEventHash;
_sc._defVarHash = {
    _pageName: "pageName,prop45,eVar45",
    _end: ""
};
_sc._defEventHash = {
    _end: ""
};
_sc._siteId = _sc._siteId || "";
/*!************************* DO PLUGIN SECTION **************************/
s.usePlugins = true;
s._fst_request = true;
if (navigator.userAgent.match(/Google Web Preview/i)) {
    s.t = function() {
        return ""
    }
}(function() {
    var i, result = false;
    for (i = 0; i < _screq.remove_becon.length; i++) {
        if (_screq.remove_becon[i] == _screq.pageName) {
            result = true
        }
    }
    if (result) {
        s.t = function() {
            return ""
        }
    }
})();

function s_doPlugins(s) {
    var _s = s._s = s._s || {};
    _sc._linkTrackAddCommonVariables();
    _s = _sc._scTempTransfer(_s);
    var _tmp, _tmp2, i, j;
    s.trancefer_tmp();
    var sclh = location.pathname;
    if (sclh.match(/\/MarginCheckAgreement/)) {
        s.pageName = (location.protocol + "//" + location.hostname + location.pathname)
    } else {
        if (sclh.match(/\/FuturesCheckAgreementAction/)) {
            s.pageName = (location.protocol + "//" + location.hostname + location.pathname)
        } else {
            if (sclh.match(/\/FxdCheckAgreement/)) {
                s.pageName = (location.protocol + "//" + location.hostname + location.pathname)
            } else {
                if (sclh.match(/\/FxcCheckAgreement/)) {
                    s.pageName = (location.protocol + "//" + location.hostname + location.pathname)
                } else {
                    if (sclh.match(/\/EntryPdfDone/)) {
                        s.pageName = (location.protocol + "//" + location.hostname + location.pathname)
                    } else {
                        if (sclh.match(/\/KoCheckAgreementAction/)) {
                            s.pageName = (location.protocol + "//" + location.hostname + location.pathname)
                        } else {
                            if (sclh.match(/\/SpPersonInfoEntry/)) {
                                s.pageName = (location.protocol + "//" + location.hostname + location.pathname)
                            } else {
                                if (sclh.match(/\/FxCheckAgreement\.do/)) {
                                    s.pageName = (location.protocol + "//" + location.hostname + location.pathname)
                                } else {
                                    if (sclh.match(/\/FxSucceedCheckAgreement\.do/)) {
                                        s.pageName = (location.protocol + "//" + location.hostname + location.pathname)
                                    } else {
                                        if (sclh.match(/\/TeFundCheckAgreementAction\.do/)) {
                                            s.pageName = (location.protocol + "//" + location.hostname + location.pathname)
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    var scprd = "";
    var scqty = "";
    var scprice = "";
    var cnt = 0;
    if (s.pageName == "TrnTrdStkCfmK0" || s.pageName == "TrnTrdStkCfmU0" || s.pageName == "TrnTrdMgnCfmK0" || s.pageName == "TrnTrdMgnCfmU0" || s.pageName == "TrnTrdRpyCfmK0" || s.pageName == "TrnTrdRpyCfmU0") {
        scprd = document.forms[s.pageName].stock_sec_code.value;
        if (document.forms[s.pageName].quantity) {
            scqty = document.forms[s.pageName].quantity.value
        } else {
            if (document.forms[s.pageName].quantity_total) {
                scqty = document.forms[s.pageName].quantity_total.value
            }
        }
        scprice = "";
        cnt = 0;
        for (i = 0; i < document.getElementsByTagName("span").length; i++) {
            if (document.getElementsByTagName("span")[i].className == "txtb") {
                if (cnt == 1) {
                    _tmp = document.getElementsByTagName("span")[i].innerHTML.match(/\d/g);
                    if (_tmp) {
                        scprice = _tmp.join("")
                    } else {
                        scprice = "0"
                    }
                } else {
                    cnt = cnt + 1
                }
            }
        }
        if (scprd && scqty && scprice) {
            s.c_w("s_pd", ";" + scprd + ";" + scqty + ";" + (scqty * scprice), 0)
        }
    }
    switch (s.pageName) {
        case "TrnTrdStkFixK0":
            s.events = "purchase";
            s.products = s.c_r("s_pd");
            s.prop30 = "SMT";
            s.eVar30 = "SMT";
            break;
        case "TrnTrdStkFixU0":
            try {
                _tmp = s.c_r("s_pd").split(";");
                s.events = "event59";
                s.products = _tmp[0] + ";" + _tmp[1];
                s.prop30 = "SMT";
                s.eVar30 = "SMT"
            } catch (e) {}
            break;
        case "TrnTrdMgnFixK0":
            try {
                _tmp = s.c_r("s_pd").split(";");
                s.events = "event21,event22";
                s.products = _tmp[0] + ";" + _tmp[1] + ";;;event22=" + _tmp[3];
                s.prop33 = "L";
                s.eVar33 = "L";
                s.prop30 = "SMT";
                s.eVar30 = "SMT"
            } catch (e) {}
            break;
        case "TrnTrdMgnFixU0":
            try {
                _tmp = s.c_r("s_pd").split(";");
                s.events = "event21,event22";
                s.products = _tmp[0] + ";" + _tmp[1] + ";;;event22=" + _tmp[3];
                s.prop33 = "S";
                s.eVar33 = "S";
                s.prop30 = "SMT";
                s.eVar30 = "SMT"
            } catch (e) {}
            break;
        case "TrnTrdMgnFixK0":
            try {
                _tmp = s.c_r("s_pd").split(";");
                s.events = "event60";
                s.products = _tmp[0] + ";" + _tmp[1];
                s.prop33 = "L";
                s.eVar33 = "L";
                s.prop30 = "SMT";
                s.eVar30 = "SMT"
            } catch (e) {}
            break;
        case "TrnTrdMgnFixU0":
            try {
                _tmp = s.c_r("s_pd").split(";");
                s.events = "event60";
                s.products = _tmp[0] + ";" + _tmp[1];
                s.prop33 = "S";
                s.prop30 = "SMT"
            } catch (e) {}
            break
    }
    var branch_ck = s.getCk("nmr_branch_code");
    var branch_ck_code = branch_ck.match(/^([^,]*)/);
    if (branch_ck && branch_ck_code) {
        s.prop38 = branch_ck_code[0];
        s.eVar38 = "D=c38"
    }
    _tmp = location.pathname;
    if (_tmp.match(/^\/branch\/branch\/([^\/]*)/) && !_tmp.match(/^\/branch\/branch\/index\.htm/) && !_tmp.match(/^\/branch\/branch\/$/)) {
        s.events = s.apl(s.events, "event36", ",", 1)
    }
    _tmp = window.trace_p;
    if (_tmp && _tmp.match(/PROS0001/)) {
        s.events = s.apl(s.events, "event34", ",", 1)
    }
    _tmp = window.trace_p;
    if (_tmp && _tmp.match(/PROS0007/)) {
        s.events = s.apl(s.events, "event35", ",", 1)
    }
    switch (_screq.pageName) {
        case "MFTRDSTKACTBUYACCEPT":
            s.events = s.apl(s.events, "event37", ",", 1);
            break;
        case "MFTRDSTKMGNBUYACCEPT":
            s.events = s.apl(s.events, "event38", ",", 1);
            break;
        case "MFTRDSTKMGNSELACCEPT":
            s.events = s.apl(s.events, "event39", ",", 1);
            break;
        case "MFTRDSTKIPODOFACCEPT":
            s.events = s.apl(s.events, "event40", ",", 1);
            break;
        case "MFTRDSTKDCTOFRACCEPT":
            s.events = s.apl(s.events, "event41", ",", 1);
            break;
        case "MFTRDSTKPRMOFRACCEPT":
            s.events = s.apl(s.events, "event42", ",", 1);
            break;
        case "MFTRDDFDDFDBUYACCEPT":
            s.events = s.apl(s.events, "event43", ",", 1);
            break;
        case "MFTRDFFDFFDBUYACCEPT":
            s.events = s.apl(s.events, "event44", ",", 1);
            break;
        case "MFTRDFFDFMFBUYACCEPT":
            s.events = s.apl(s.events, "event45", ",", 1);
            break;
        case "MFTRDDBDDBDBUYACCEPT":
            s.events = s.apl(s.events, "event46", ",", 1);
            break;
        case "MFACTCUSETDOFRACCEPT":
            s.events = s.apl(s.events, "event47", ",", 1);
            break;
        case "MFACTCUSDCCOFRACCEPT":
            s.events = s.apl(s.events, "event48", ",", 1);
            break;
        case "MFACTCUSMGAEOFACCEPT":
            s.events = s.apl(s.events, "event49", ",", 1);
            break;
        case "MFACTCUSNTBEOFACCEPT":
            s.events = s.apl(s.events, "event50", ",", 1);
            break;
        case "MFACTAVSVSVOFRACCEPT":
            s.events = s.apl(s.events, "event92", ",", 1);
            break;
        case "SMT-MFTRDSTKACTBUYACCEPT":
            s.events = s.apl(s.events, "event37", ",", 1);
            break;
        case "SMT-MFTRDSTKMGNBUYACCEPT":
            s.events = s.apl(s.events, "event38", ",", 1);
            break;
        case "SMT-MFTRDSTKMGNSELACCEPT":
            s.events = s.apl(s.events, "event39", ",", 1);
            break;
        case "SMT-MFTRDDBDDBDBUYACCEPT":
            s.events = s.apl(s.events, "event46", ",", 1);
            break;
        case "RMFCMNETCINQACCEPT":
            s.events = s.apl(s.events, "event105", ",", 1);
            break;
        case "RMFTRDSTKODDBUYACCEPT":
            s.events = s.apl(s.events, "event106", ",", 1);
            break;
        case "RMFTRDSTKOFSODRACCEPT":
            s.events = s.apl(s.events, "event107", ",", 1);
            break;
        case "RMFTRDSTKTOBOFRACCEPT":
            s.events = s.apl(s.events, "event108", ",", 1);
            break;
        case "RMFTRDDFDDFRBUYACCEPT":
            s.events = s.apl(s.events, "event109", ",", 1);
            break;
        case "RMFTRDFRCFRCOFRACCEPT":
            s.events = s.apl(s.events, "event110", ",", 1);
            break
    }
    if (s.prop26) {
        s.eVar26 = "D=c26"
    }
    if (s.prop27) {
        s.eVar27 = "D=c27";
        s.setCk("sc_ht", s.prop27, 365);
        s.setCk("sc_htl", "T")
    }
    if (_screq.pageName == "MFCMNCAUSYSLGO" || _screq.pageName == "SMT-MFCMNCAUSYSLGO") {
        s.setCk("sc_htl", "", -1)
    }
    if (s.pageType) {
        s.pageName = ""
    } else {
        if (s.pageName == "404_notfound" || s.pageName == "illegal" || (s.pageName && s.pageName.match(/ERROR404/))) {
            s.pageName = "";
            s.pageType = "errorPage"
        } else {
            if (s.pageName && s.pageName.match(/^[^0-9a-zA-Z\/:\[\]{}]/)) {
                s.pageName = location.href.split("?")[0]
            } else {
                if (s.pageName && !s.pageName.match(/^(pc|sp|tb)=:/)) {
                    _tmp = location.host.split(".nomura.co.jp")[0];
                    _tmp2 = s.pageName.split("=/");
                    s.pageName = _tmp + "=/" + _tmp2[_tmp2.length - 1]
                } else {
                    s.pageName = location.href.split("?")[0]
                }
            }
        }
    }
    if (s.pageName && s.pageName.match(/\/$/)) {
        s.pageName = s.pageName + "index.html"
    }
    if (!s.pageType && window.trace_p) {
        s.pageName += "/:" + window.trace_p
    }
    _tmp = s.getQueryParam("cat1");
    _tmp2 = s.getQueryParam("cat2");
    if (!s.pagetype && (_tmp || _tmp2)) {
        s.pageName += "/cat1=" + _tmp + ":cat2=" + _tmp2
    }
    if (s.events && s.events.match("event85")) {
        s.events = s.apl(s.events, "event86", ",", 1)
    }
    if (s.events && s.events.match("event87")) {
        s.events = s.apl(s.events, "event88", ",", 1)
    }
    _s._pageName = s.pageName;
    if (_screq.prop51) {
        s.eVar51 = "D=c51"
    }
    if (s._fst_request) {
        _s = _sc._s_doOnce(_s)
    }
    _sc._varsCopybackToS(_s);
    if (s.c_r("crm_id") === "" && s.prop51 !== undefined) {
        (function() {
            return function(c_name, value, domain) {
                var path = "/";
                var s = c_name + "=" + escape(value) + "; path=" + path + "; domain=" + domain;
                document.cookie = s
            }
        })()("crm_id", s.prop51, document.domain)
    }
}
s.doPlugins = s_doPlugins;
try {
    var currentScript = (function(e) {
        if (e.nodeName.toLowerCase() == "script") {
            return e
        }
        return arguments.callee(e.lastChild)
    })(document);
    if (currentScript) {
        s.prop48 = currentScript.src
    }
} catch (err) {}
_sc._s_doOnce = function(_s) {
    var _tmp, _tmp2, i, j;
    s._inbound_ids = [
        ["sc_sid", "Lis"],
        ["cid", "Lis"],
        ["wid", "Lis"],
        ["sc_afid", "Aff"],
        ["sc_bid", "Ban"],
        ["banner_id", "Ban"],
        ["sc_mid", "Mai"],
        ["sc_iid", "Inf"],
        ["sc_apid", "App"],
        ["sc_zid", "OId"]
    ];
    s._ownSiteList = ["nomura-trade-i.ne.jp", "net-ir.ne.jp", "postub.com", "billingjapan.co.jp", "nomura-am.co.jp", "nomura.com", "nomura-daiichilife.jp", "denshikofu.nomura.co.jp", "my.nomura.co.jp", "fwg.ne.jp"];
    s._socialSiteList = ["facebook.com", "mixi.jp", "twitter.com", "nomura.jp", "linkedin.com", "gree.jp", "plus.google.com", /^t.co$/];
    s._blogSiteList = ["2ch.net", "ameba.jp", "ameblo.jp", "webryblog.biglobe.ne.jp", "blog.goo.ne.jp", "blogs.yahoo.co.jp", "blogspot.com", "cocolog-nifty.com", "dtiblog.com", "exblog.jp", "fc2.com", "geocities.jp", "ime.nu", "jugem.jp", "blog.livedoor.com", "seesaa.net", "teacup.com", "hatena.ne.jp", "yaplog.jp", "chiebukuro.yahoo.co.jp", "okwave.jp", "oshiete.goo.ne.jp", "oshiete.nikkeibp.co.jp", "questionbox.jp.msn.com", "qanda.rakuten.co.jp", "soudan.biglobe.ne.jp"];
    for (i = 0; i < s._inbound_ids.length; i++) {
        _tmp = s.getQueryParam(s._inbound_ids[i][0]);
        if (_tmp) {
            s.campaign = s._inbound_ids[i][0] + ":" + _tmp;
            s.eVar17 = "D=v0";
            s.eVar18 = "D=v0";
            break
        }
    }
    s._fst_page = s.get1stOr2ndpage();
    if (s._fst_page == 1) {
        s.events = s.apl(s.events, "event19", ",", 1);
        s.channelManager();
        for (i = 0; i < s._inbound_ids.length; i++) {
            _tmp = s.getQueryParam(s._inbound_ids[i][0]);
            if (_tmp) {
                s.eVar12 = s._inbound_ids[i][1];
                s.eVar13 = s.eVar12 + ":" + _tmp;
                break
            }
        }
        if (!s.eVar12 && s._channel == "Natural Search") {
            s.eVar12 = "Org";
            s.eVar13 = "Org:" + s._campaign
        }
        if (!s.eVar12 && s._channel == "Referrers") {
            for (i = 0; i < s._ownSiteList.length; i++) {
                if (s._referringDomain.match(s._ownSiteList[i])) {
                    s.eVar12 = "Own";
                    s.eVar13 = "Own:" + s._campaign;
                    break
                }
            }
        }
        if (!s.eVar12 && s._channel == "Referrers") {
            for (i = 0; i < s._socialSiteList.length; i++) {
                if (s._referringDomain.match(s._socialSiteList[i])) {
                    s.eVar12 = "Soc";
                    s.eVar13 = "Soc:" + s._campaign;
                    break
                }
            }
        }
        if (!s.eVar12 && s._channel == "Referrers") {
            for (i = 0; i < s._blogSiteList.length; i++) {
                if (s._referringDomain.match(s._blogSiteList[i])) {
                    s.eVar12 = "UGC";
                    s.eVar13 = "UGC:" + s._campaign;
                    break
                }
            }
        }
        if (!s.eVar12 && s._channel === "") {
            for (i = 0; i < s._linkInternalFiltersArr.length; i++) {
                if (document.referrer.match(s._linkInternalFiltersArr[i])) {
                    s.eVar12 = "Int";
                    s.eVar13 = "Int";
                    break
                }
            }
        }
        if (!s.eVar12 && s._channel == "Referrers") {
            s.eVar12 = "OSi";
            s.eVar13 = "OSi:" + s._campaign
        }
        if (!s.eVar12 && !document.referrer) {
            s.eVar12 = "Dir";
            s.eVar13 = "Dir"
        }
    } else {
        if (s._fst_page == 2) {
            s.events = s.apl(s.events, "event20", ",", 1)
        }
    }
    if (!s.eVar12) {
        s.eVar12 = "Int";
        s.eVar13 = "Int"
    }
    s.server = location.host;
    _tmp = location.host.split(".nomura.co.jp")[0];
    if (s.channel == "p" || s.channel == "s") {
        s.channel = location.host.split(".nomura.co.jp")[0];
        s.channel += "="
    } else {
        if (_tmp.match(/trading/) || _tmp.match(/quick/) || _tmp.match(/account/) || _tmp.match(/nc/) || _tmp.match(/smart/) || _tmp.match(/virtualfx/)) {
            s.channel = location.host.split(".nomura.co.jp")[0];
            s.channel += "="
        } else {
            if (!s.channel) {
                _tmp = s.getPageName().split("/");
                s.channel = location.host.split(".nomura.co.jp")[0];
                i = 0;
                s.channel += "=";
                while (i < 3 && i < _tmp.length) {
                    s.channel = s.channel + "/" + _tmp[i];
                    i++
                }
            }
        }
    }
    if (s._fst_page == 1) {
        s.eVar14 = "D=pageName";
        s.eVar15 = "D=r"
    }
    if (!s.hier1) {
        s.hier1 = s.pageName;
        s.hier1 = s.hier1.replace(/http?:\/\//, "");
        s.hier1 = s.hier1.replace(/\=:/, "/")
    }
    if (s._fst_page == 1 && s.eVar12) {
        s.prop5 = 'D="' + s.eVar12 + ':"+pageName'
    } else {
        s.prop5 = "D=pageName"
    }
    if (_screq.prop4) {
        s.eVar4 = "D=c4"
    }
    if (_screq.prop30) {
        s.eVar30 = "D=c30"
    }
    if (_screq.prop33) {
        s.eVar33 = "D=c33"
    }
    if (_screq.prop34) {
        s.eVar34 = "D=c34"
    }
    if (_screq.prop35) {
        s.eVar35 = "D=pageName"
    }
    s.eVar6 = s.getNewRepeat(365, "sc_nr");
    if (s.eVar6 == "New") {
        s.prop6 = "D=pageName"
    } else {
        if (s.eVar6 == "Repeat") {
            s.prop7 = "D=pageName"
        }
    }
    _tmp = "";
    _tmp = s.getCk("ac");
    if (_tmp && _tmp != "_") {
        s.prop2 = _tmp;
        s.eVar2 = "D=c2";
        s.setCk("sc_ncl", "T")
    }
    _tmp2 = "";
    _tmp2 = s.getPreviousValue(_tmp, "sc_bck");
    if (_tmp2 == "_" && _tmp != "_") {
        s.events = s.apl(s.events, "event3", ",", 1)
    }
    s.eVar16 = s.getQueryParam("kzid");
    s.prop20 = "D=s_vi";
    s.eVar20 = "D=s_vi";
    s.prop21 = s.getTimeParting("h", 9, new Date().getFullYear());
    s.prop22 = s.getTimeParting("d", 9, new Date().getFullYear());
    s.prop23 = s.getTimeParting("w", 9, new Date().getFullYear());
    s.eVar21 = "D=c21";
    s.eVar22 = "D=c22";
    s.eVar23 = "D=c23";
    s.prop24 = s.getPreviousValue(s.pageName, "sc_prePageName");
    s.eVar24 = "D=c24";
    if (window.optimostLayoutID) {
        s.prop29 = window.optimostLayoutID;
        s.eVar29 = "D=c29"
    }
    if (window.optimostSegmentID) {
        s.prop31 = window.optimostSegmentID;
        s.eVar31 = "D=c31"
    }
    s.prop43 = navigator.userAgent;
    s.eVar43 = "D=c43";
    s.prop44 = _sc.getUtf100Byte(document.title);
    s.prop46 = "D=g";
    s.prop47 = "D=r";
    if (!s.eVar11) {
        s.eVar11 = s.getValOnce(s.getQueryParam("sc_pid").split("#")[0], "sc_pid", 0)
    }
    if (s.events && ((_screq.pageName && _screq.pageName.match(/^SMT-/)) || (s.pageName.match(/sp=:/)))) {
        s.prop28 = "SMT";
        s.eVar28 = "D=c28"
    } else {
        if (s.events) {
            s.prop28 = "PC";
            s.eVar28 = "D=c28"
        }
    }
    s.prop50 = s.s_code_version;
    s._fst_request = false;
    s.plugins = "track_off";
    return _s
};
/*!************************* Utitity Funciton  *************************/
_sc._defShortName = {
    campaign: "v0",
    pageName: "pageName",
    server: "server",
    channel: "ch",
    purchaseID: "purchaseID",
    transactionID: "transactionID",
    events: "events",
    products: "products",
    visitorID: "vid",
    list1: "list1",
    list2: "list2",
    list3: "list3",
    hier1: "h1",
    referrer: "r",
    pageURL: "g"
};
_sc._merge = function(obj1, obj2) {
    var attrname;
    if (!obj2) {
        obj2 = {}
    }
    for (attrname in obj2) {
        if (obj2.hasOwnProperty(attrname)) {
            obj1[attrname] = obj2[attrname]
        }
    }
    return obj1
};
_sc.getUtf100Byte = function(str) {
    var len, result;
    result = str;
    len = result.length;
    while (_sc.getUtf8Byte(result) > 100) {
        result = result.substring(0, len - 1);
        len = result.length
    }
    return result
};
_sc.getUtf8Byte = function(str) {
    var ue, per, len;
    try {
        ue = encodeURI(str);
        per = ue.match(/%/g);
        if (per) {
            len = ue.length - per.length * 2
        } else {
            len = ue.length
        }
        return len
    } catch (e) {
        return nil
    }
};
_sc._varsCopybackToS = function(_s) {
    var i, item, defvar_split_ary, tmp2, k, regEx;
    var shortName = _sc._defShortName;
    for (i in _s) {
        if (typeof s[i] != "object" && typeof s[i] != "function") {
            s[i] = _s[i]
        }
    }
    for (item in _sc._defVarHash) {
        defvar_split_ary = _sc._defVarHash[item].split(",");
        for (i = 0; i < defvar_split_ary.length; i++) {
            if (_s[item]) {
                s[defvar_split_ary[i]] = _s[item];
                if (i > 0 && !(_s[item].toString().match("D="))) {
                    if ((tmp2 = defvar_split_ary[0].match(/prop(.*)/))) {
                        s[defvar_split_ary[i]] = "D=c" + tmp2[1];
                        break
                    }
                    if ((tmp2 = defvar_split_ary[0].match(/eVar(.*)/))) {
                        s[defvar_split_ary[i]] = "D=v" + tmp2[1];
                        break
                    }
                    for (k in shortName) {
                        regEx = new RegExp("^" + k + "$");
                        if ((tmp2 = defvar_split_ary[0].match(regEx))) {
                            s[defvar_split_ary[i]] = "D=" + shortName[k];
                            break
                        }
                    }
                }
            } else {
                if (i > 0 && s[defvar_split_ary[0]] && !(s[defvar_split_ary[0]].toString().match("D="))) {
                    if ((tmp2 = defvar_split_ary[0].match(/prop(.*)/))) {
                        s[defvar_split_ary[i]] = "D=c" + tmp2[1]
                    }
                    if ((tmp2 = defvar_split_ary[0].match(/eVar(.*)/))) {
                        s[defvar_split_ary[i]] = "D=v" + tmp2[1]
                    }
                }
            }
        }
    }
    if (_sc._debugMode === "T") {}
    return s
};
_sc._scTempTransfer = function(_s) {
    var i, j, item, defvar_split_ary;
    var _sc = window._sc;
    var shortName = _sc._defShortName;
    if (_sc._debugMode === "T" && _sc._repetition && window.console) {
        console.log("_sc._repetition:" + _sc._repetition)
    }
    for (i in _sc) {
        if (typeof _sc[i] != "object" && typeof _sc[i] != "function") {
            _s[i] = _sc[i]
        }
        for (item in _sc._defVarHash) {
            defvar_split_ary = _sc._defVarHash[item].split(",");
            for (j = 0; j < defvar_split_ary.length; j++) {
                if (defvar_split_ary[j] == i) {
                    _s[item] = _sc[i];
                    if (_sc._debugMode === "T" && window.console) {
                        console.log("_s[" + item + "]:" + _s[item])
                    }
                    break
                }
            }
        }
    }
    return _s
};
_sc._linkTrackResetCommonVariables = function() {
    var i, tmp, tmp2;
    tmp = "";
    _sc.linkTrackVars = "";
    _sc.linkTrackEvents = "";
    try {
        for (i in _sc._defVarHash) {
            if ((i.match(/^c_/) || i.match(/^_/)) && _sc._defVarHash[i]) {
                tmp += "," + _sc._defVarHash[i]
            }
        }
        tmp2 = tmp.substr(1, tmp.length - 1);
        if (_sc.linkTrackVars) {
            if (!_sc.linkTrackVars.match(tmp2)) {
                _sc.linkTrackVars += tmp
            }
        } else {
            _sc.linkTrackVars = tmp2
        }
    } catch (e) {}
};
_sc._linkTrackAddCommonVariables = function() {
    var i, tmp, tmp2;
    tmp = "";
    _sc.linkTrackVars = _sc.linkTrackVars || "";
    _sc.linkTrackEvents = _sc.linkTrackEvents || "";
    try {
        for (i in _sc._defVarHash) {
            if ((i.match(/^c_/) || i.match(/^_/)) && _sc._defVarHash[i]) {
                tmp += "," + _sc._defVarHash[i]
            }
        }
        tmp2 = tmp.substr(1, tmp.length - 1);
        if (_sc.linkTrackVars) {
            if (!_sc.linkTrackVars.match(tmp2)) {
                _sc.linkTrackVars += tmp
            }
        } else {
            _sc.linkTrackVars = tmp2
        }
    } catch (e) {}
};
/*!************************* Global Funciton  *************************/
s.customCvPdfDownload = function(that, pdfurl) {
    s.tl(that, "d", pdfurl)
};
s.branchTelClick = function() {
    var isDeviceFlag;
    if (window.nmr) {
        nmr.onchange({
            onSmall: function() {
                isDeviceFlag = true
            },
            offSmall: function() {
                isDeviceFlag = false
            }
        })
    }
    if (isDeviceFlag) {
        s.linkTrackVars = "events,prop38,eVar38";
        s.linkTrackEvents = "event84";
        s.events = "event84";
        s.tl("dummy", "o", "branch")
    }
};
s.branchSearch = function(that) {
    s.linkTrackVars = "prop37,eVar37";
    try {
        s.prop37 = that.form["data[SeminarSearch][search_keyword]"].value;
        s.eVar37 = "D=c37"
    } catch (e) {}
    s.tl(that, "o", "branchsearch");
    return true
};
_sc.clickEvent = function(linkname, events, products, varHash) {
    varHash = varHash ? varHash : {};
    var i, j, item, defvar_split_ary, tmp2, k, regEx, key;
    var shortName = _sc._defShortName;
    var varHashKeys = [],
        num;
    for (i in varHash) {
        varHashKeys.push(i);
        _sc[i] = varHash[i]
    }
    events = events ? events : "";
    products = products ? products : "";
    _sc.linkTrackVars = "events";
    if (products) {
        _sc.linkTrackVars += ",products"
    }
    if (varHashKeys != []) {
        for (item in _sc._defVarHash) {
            defvar_split_ary = _sc._defVarHash[item].split(",");
            if (varHash[item]) {
                for (i = 0; i < defvar_split_ary.length; i++) {
                    _sc.linkTrackVars = s.apl(_sc.linkTrackVars, defvar_split_ary[i], ",", 1)
                }
            } else {
                for (i = 0; i < defvar_split_ary.length; i++) {
                    for (j = 0; j < varHashKeys.length; j++) {
                        if (varHashKeys[j] === defvar_split_ary[i]) {
                            for (k = 0; k < defvar_split_ary.length; k++) {
                                _sc.linkTrackVars = s.apl(_sc.linkTrackVars, defvar_split_ary[k], ",", 1)
                            }
                        }
                    }
                }
            }
        }
        for (i in varHash) {
            num = i.match(/prop(.*)/);
            if (num) {
                _sc.linkTrackVars = s.apl(_sc.linkTrackVars, "prop" + num[1], ",", 1)
            }
            num = i.match(/eVar(.*)/);
            if (num) {
                _sc.linkTrackVars = s.apl(_sc.linkTrackVars, "eVar" + num[1], ",", 1)
            }
        }
    }
    _sc.linkTrackEvents = events;
    _sc.events = events;
    _sc.products = products;
    s.tl("dummy", "o", _sc._siteId + ":" + linkname);
    _sc._linkTrackResetCommonVariables()
};
if (_sc._defVarHash && _sc._defVarHashBefore) {
    _sc._merge(_sc._defVarHash, _sc._defVarHashBefore)
}
if (_sc._defEventHash && _sc._defEventHashBefore) {
    _sc._merge(_sc._defEventHash, _sc._defEventHashBefore);
    /*!************************* PLUGINS SECTION *************************/
}
if (window.s) {
    s.wd = window
}
if (!s.getQueryParam) {
    s.getQueryParam = function(key, delim, url) {
        var target;
        if (url) {
            target = url.split("#")[0]
        } else {
            target = location.href.split("#")[0]
        }
        return s.Util.getQueryParam(key, target, delim)
    }
}
if (!s.c_w) {
    s.c_w = window.s.Util.cookieWrite
}
if (!s.c_r) {
    s.c_r = window.s.Util.cookieRead
}
if (!s.fl) {
    s.fl = function(x, l) {
        return x ? ("" + x).substring(0, l) : x
    }
}
if (!s.pt) {
    s.pt = function(x, d, f, a) {
        var s = this,
            t = x,
            z = 0,
            y, r;
        while (t) {
            y = t.indexOf(d);
            y = y < 0 ? t.length : y;
            t = t.substring(0, y);
            r = s[f](t, a);
            if (r) {
                return r
            }
            z += y + d.length;
            t = x.substring(z, x.length);
            t = z < x.length ? t : ""
        }
        return ""
    }
}
s.hav = function() {
    var s = this,
        qs = "",
        l, fv = "",
        fe = "",
        mn, i, e;
    if (s.lightProfileID) {
        l = s.va_m;
        fv = s.lightTrackVars;
        if (fv) {
            fv = "," + fv + "," + s.vl_mr + ","
        }
    } else {
        l = s.va_t;
        if (s.pe || s.linkType) {
            fv = s.linkTrackVars;
            fe = s.linkTrackEvents;
            if (s.pe) {
                mn = s.pe.substring(0, 1).toUpperCase() + s.pe.substring(1);
                if (s[mn]) {
                    fv = s[mn].trackVars;
                    fe = s[mn].trackEvents
                }
            }
        }
        if (fv) {
            fv = "," + fv + "," + s.vl_l + "," + s.vl_l2
        }
        if (fe) {
            fe = "," + fe + ",";
            if (fv) {
                fv += ",events,"
            }
        }
        if (s.events2) {
            e = (e ? "," : "") + s.events2
        }
    }
    for (i = 0; i < l.length; i++) {
        var k = l[i],
            v = s[k],
            b = k.substring(0, 4),
            x = k.substring(4),
            n = parseInt(x),
            q = k;
        if (!v) {
            if (k == "events" && e) {
                v = e;
                e = ""
            }
        }
        if (v && (!fv || ("," + fv + ",").indexOf("," + k + ",") >= 0) && k != "linkName" && k != "linkType") {
            if (k == "timestamp") {
                q = "ts"
            } else {
                if (k == "dynamicVariablePrefix") {
                    q = "D"
                } else {
                    if (k == "visitorID") {
                        q = "vid"
                    } else {
                        if (k == "pageURL") {
                            q = "g";
                            v = s.fl(v, 255)
                        } else {
                            if (k == "referrer") {
                                q = "r";
                                v = s.fl(s.rf(v), 255)
                            } else {
                                if (k == "vmk" || k == "visitorMigrationKey") {
                                    q = "vmt"
                                } else {
                                    if (k == "visitorMigrationServer") {
                                        q = "vmf";
                                        if (s.ssl && s.visitorMigrationServerSecure) {
                                            v = ""
                                        }
                                    } else {
                                        if (k == "visitorMigrationServerSecure") {
                                            q = "vmf";
                                            if (!s.ssl && s.visitorMigrationServer) {
                                                v = ""
                                            }
                                        } else {
                                            if (k == "charSet") {
                                                q = "ce";
                                                if (v.toUpperCase() == "AUTO") {
                                                    v = "ISO8859-1"
                                                } else {
                                                    if (s.em == 2 || s.em == 3) {
                                                        v = "UTF-8"
                                                    }
                                                }
                                            } else {
                                                if (k == "visitorNamespace") {
                                                    q = "ns"
                                                } else {
                                                    if (k == "cookieDomainPeriods") {
                                                        q = "cdp"
                                                    } else {
                                                        if (k == "cookieLifetime") {
                                                            q = "cl"
                                                        } else {
                                                            if (k == "variableProvider") {
                                                                q = "vvp"
                                                            } else {
                                                                if (k == "currencyCode") {
                                                                    q = "cc"
                                                                } else {
                                                                    if (k == "channel") {
                                                                        q = "ch"
                                                                    } else {
                                                                        if (k == "transactionID") {
                                                                            q = "xact"
                                                                        } else {
                                                                            if (k == "campaign") {
                                                                                q = "v0"
                                                                            } else {
                                                                                if (k == "resolution") {
                                                                                    q = "s"
                                                                                } else {
                                                                                    if (k == "colorDepth") {
                                                                                        q = "c"
                                                                                    } else {
                                                                                        if (k == "javascriptVersion") {
                                                                                            q = "j"
                                                                                        } else {
                                                                                            if (k == "javaEnabled") {
                                                                                                q = "v"
                                                                                            } else {
                                                                                                if (k == "cookiesEnabled") {
                                                                                                    q = "k"
                                                                                                } else {
                                                                                                    if (k == "browserWidth") {
                                                                                                        q = "bw"
                                                                                                    } else {
                                                                                                        if (k == "browserHeight") {
                                                                                                            q = "bh"
                                                                                                        } else {
                                                                                                            if (k == "connectionType") {
                                                                                                                q = "ct"
                                                                                                            } else {
                                                                                                                if (k == "homepage") {
                                                                                                                    q = "hp"
                                                                                                                } else {
                                                                                                                    if (k == "plugins") {
                                                                                                                        q = "p"
                                                                                                                    } else {
                                                                                                                        if (k == "events") {
                                                                                                                            if (e) {
                                                                                                                                v += (v ? "," : "") + e
                                                                                                                            }
                                                                                                                            if (fe) {
                                                                                                                                v = s.fs(v, fe)
                                                                                                                            }
                                                                                                                        } else {
                                                                                                                            if (k == "events2") {
                                                                                                                                v = ""
                                                                                                                            } else {
                                                                                                                                if (k == "contextData") {
                                                                                                                                    qs += s.s2q("c", s[k], fv, k, 0);
                                                                                                                                    v = ""
                                                                                                                                } else {
                                                                                                                                    if (k == "lightProfileID") {
                                                                                                                                        q = "mtp"
                                                                                                                                    } else {
                                                                                                                                        if (k == "lightStoreForSeconds") {
                                                                                                                                            q = "mtss";
                                                                                                                                            if (!s.lightProfileID) {
                                                                                                                                                v = ""
                                                                                                                                            }
                                                                                                                                        } else {
                                                                                                                                            if (k == "lightIncrementBy") {
                                                                                                                                                q = "mti";
                                                                                                                                                if (!s.lightProfileID) {
                                                                                                                                                    v = ""
                                                                                                                                                }
                                                                                                                                            } else {
                                                                                                                                                if (k == "retrieveLightProfiles") {
                                                                                                                                                    q = "mtsr"
                                                                                                                                                } else {
                                                                                                                                                    if (k == "deleteLightProfiles") {
                                                                                                                                                        q = "mtsd"
                                                                                                                                                    } else {
                                                                                                                                                        if (k == "retrieveLightData") {
                                                                                                                                                            if (s.retrieveLightProfiles) {
                                                                                                                                                                qs += s.s2q("mts", s[k], fv, k, 0)
                                                                                                                                                            }
                                                                                                                                                            v = ""
                                                                                                                                                        } else {
                                                                                                                                                            if (s.num(x)) {
                                                                                                                                                                if (b == "prop") {
                                                                                                                                                                    q = "c" + n
                                                                                                                                                                } else {
                                                                                                                                                                    if (b == "eVar") {
                                                                                                                                                                        q = "v" + n
                                                                                                                                                                    } else {
                                                                                                                                                                        if (b == "list") {
                                                                                                                                                                            q = "l" + n
                                                                                                                                                                        } else {
                                                                                                                                                                            if (b == "hier") {
                                                                                                                                                                                q = "h" + n;
                                                                                                                                                                                v = s.fl(v, 255)
                                                                                                                                                                            }
                                                                                                                                                                        }
                                                                                                                                                                    }
                                                                                                                                                                }
                                                                                                                                                            }
                                                                                                                                                        }
                                                                                                                                                    }
                                                                                                                                                }
                                                                                                                                            }
                                                                                                                                        }
                                                                                                                                    }
                                                                                                                                }
                                                                                                                            }
                                                                                                                        }
                                                                                                                    }
                                                                                                                }
                                                                                                            }
                                                                                                        }
                                                                                                    }
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (v) {
                qs += "&" + s.ape(q) + "=" + (k.substring(0, 3) != "pev" ? s.ape(v) : v)
            }
        }
    }
    return qs
};
s.get1stOr2ndpage = function() {
    if (s.getVisitStart("sc_visit")) {
        s.c_w("sc_fs", "ld", 0);
        s.c_w("sc_fspage", location.href, 0);
        return 1
    } else {
        if (s.c_r("sc_fs") == "ld" && location.href != s.c_r("sc_fspage")) {
            s.c_w("sc_fs", "", -1);
            s.c_w("sc_fspage", "", -1);
            return 2
        }
    }
    return 0
};
s.checkVariables = function(prop) {
    var e = new Array();
    var ev = new Array();
    var p = new Array();
    var pv = new Array();
    for (var i = 0; i < 75; i++) {
        e[i] = s["eVar" + (i + 1)];
        if (e[i] != "" && typeof(e[i]) != "undefined") {
            ev.push("v" + (i + 1))
        }
        if ("prop" + (i + 1) != prop) {
            p[i] = s["prop" + (i + 1)];
            if (p[i] != "" && typeof(p[i]) != "undefined") {
                pv.push("c" + (i + 1))
            }
        }
    }
    var pro = "";
    var eve = "";
    if (s.products) {
        pro = s.products.substring(0, 20).replace(/;/g, "_")
    }
    if (s.events) {
        eve = s.events
    }
    if (!s.campaign) {
        s.campaign = ""
    }
    prop = prop ? prop : "prop49";
    s[prop] = eve + ";" + s.campaign + ";" + ev.toString() + ";" + pv.toString() + ";" + pro;
    s.linkTrackVars = s.linkTrackVars ? s.linkTrackVars + "," + prop : ""
};
s.getTimeParting = function(t, z, y) {
    var t, z, y;
    var dc = new Date("1/1/2000");
    var f = 15;
    var ne = 8;
    if (dc.getDay() != 6 || dc.getMonth() != 0) {
        return "Data Not Available"
    } else {
        z = parseInt(z);
        if (y == "2009") {
            f = 8;
            ne = 1
        }
        var gmar = new Date("3/1/" + y);
        var dsts = f - gmar.getDay();
        var gnov = new Date("11/1/" + y);
        var dste = ne - gnov.getDay();
        var spr = new Date("3/" + dsts + "/" + y);
        var fl = new Date("11/" + dste + "/" + y);
        var cd = new Date;
        if (cd > spr && cd < fl) {
            z = z
        } else {
            z = z
        }
        var utc = cd.getTime() + cd.getTimezoneOffset() * 60000;
        var tz = new Date(utc + 3600000 * z);
        var thisy = tz.getFullYear();
        var days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
        if (thisy != y) {
            return "Data Not Available"
        } else {
            var thish = tz.getHours();
            var thismin = tz.getMinutes();
            var thisd = tz.getDay();
            var dow = days[thisd];
            var ap = "AM";
            var dt = "Weekday";
            var mint = "00";
            if (thismin > 30) {
                var mint = "30"
            }
            if (thish >= 12) {
                var ap = "PM";
                thish = thish - 12
            }
            if (thish == 0) {
                thish = 12
            }
            if (thisd == 6 || thisd == 0) {
                dt = "Weekend"
            }
            var timestring = thish + ":" + mint + ap;
            var daystring = dow;
            var endstring = dt;
            if (t == "h") {
                return timestring
            }
            if (t == "d") {
                return daystring
            }
            if (t == "w") {
                return endstring
            }
        }
    }
};
s.getVisitStart = new Function("c", "var s=this,v=1,t=new Date;t.setTime(t.getTime()+1800000);if(s.c_r(c)){v=0}if(!s.c_w(c,1,t)){s.c_w(c,1,0)}if(!s.c_r(c)){v=0}return v;");
s.siteID = "";
s.defaultPage = "index.html";
s.queryVarsList = "";
s.pathExcludeDelim = ";";
s.pathConcatDelim = "";
s.pathExcludeList = "";
s.getPageName = new Function("u", "var s=this,v=u?u:''+s.wd.location,x=v.indexOf(':'),y=v.indexOf('/',x+4),z=v.indexOf('?'),c=s.pathConcatDelim,e=s.pathExcludeDelim,g=s.queryVarsList,d=s.siteID,n=d?d:'',q=z<0?'':v.substring(z+1),p=v.substring(y+1,q?z:v.length);z=p.indexOf('#');p=z<0?p:s.fl(p,z);x=e?p.indexOf(e):-1;p=x<0?p:s.fl(p,x);p+=!p||p.charAt(p.length-1)=='/'?s.defaultPage:'';y=c?c:'/';while(p){x=p.indexOf('/');x=x<0?p.length:x;z=s.fl(p,x);if(!s.pt(s.pathExcludeList,',','p_c',z))n+=n?y+z:z;p=p.substring(x+1)}y=c?c:'?';while(g){x=g.indexOf(',');x=x<0?g.length:x;z=s.fl(g,x);z=s.pt(q,'&','p_c',z);if(z){n+=n?y+z:z;y=c?c:'&'}g=g.substring(x+1)}return n");
s.getValOnce = new Function("v", "c", "e", "var s=this,a=new Date,v=v?v:v='',c=c?c:c='s_gvo',e=e?e:0,k=s.c_r(c);if(v){a.setTime(a.getTime()+e*86400000);s.c_w(c,v,e?a:0);}return v==k?'':v");
s.getNewRepeat = new Function("d", "cn", "var s=this,e=new Date(),cval,sval,ct=e.getTime();d=d?d:30;cn=cn?cn:'s_nr';e.setTime(ct+d*24*60*60*1000);cval=s.c_r(cn);if(cval.length==0){s.c_w(cn,ct+'-New',e);return'New';}sval=s.split(cval,'-');if(ct-sval[0]<30*60*1000&&sval[1]=='New'){s.c_w(cn,ct+'-New',e);return'New';}else{s.c_w(cn,ct+'-Repeat',e);return'Repeat';}");
s.channelManager = new Function("a", "b", "c", "d", "e", "f", "var s=this,A,B,g,l,m,M,p,q,P,h,k,u,S,i,O,T,j,r,t,D,E,F,G,H,N,U,v=0,X,Y,W,n=new Date;n.setTime(n.getTime()+1800000);if(e){v=1;if(s.c_r(e)){v=0}if(!s.c_w(e,1,n)){s.c_w(e,1,0)}if(!s.c_r(e)){v=0}}g=s.referrer?s.referrer:document.referrer;g=g.toLowerCase();if(!g){h=1}i=g.indexOf('?')>-1?g.indexOf('?'):g.length;j=g.substring(0,i);k=s.linkInternalFilters.toLowerCase();k=s.split(k,',');l=k.length;for(m=0;m<l;m++){B=j.indexOf(k[m])==-1?'':g;if(B)O=B}if(!O&&!h){p=g;U=g.indexOf('//');q=U>-1?U+2:0;Y=g.indexOf('/',q);r=Y>-1?Y:i;t=g.substring(q,r);t=t.toLowerCase();u=t;P='Referrers';S=s.seList+'>'+s._extraSearchEngines;if(d==1){j=s.repl(j,'oogle','%');j=s.repl(j,'ahoo','^');g=s.repl(g,'as_q','*')}A=s.split(S,'>');T=A.length;for(i=0;i<T;i++){D=A[i];D=s.split(D,'|');E=s.split(D[0],',');F=E.length;for(G=0;G<F;G++){H=j.indexOf(E[G]);if(H>-1){i=s.split(D[1],',');U=i.length;for(k=0;k<U;k++){l=s.getQueryParam(i[k],'',g);if(l){l=l.toLowerCase();M=l;if(D[2]){u=D[2];N=D[2]}else{N=t}if(d==1){N=s.repl(N,'#',' - ');g=s.repl(g,'*','as_q');N=s.repl(N,'^','ahoo');N=s.repl(N,'%','oogle');}}}}}}}if(!O||f!='1'){O=s.getQueryParam(a,b);if(O){u=O;if(M){P='Paid Search'}else{P='Paid Non-Search';}}if(!O&&M){u=N;P='Natural Search'}}if(h==1&&!O&&v==1){u=P=t=p='Direct Load'}X=M+u+t;c=c?c:'c_m';if(c!='0'){X=s.getValOnce(X,c,0);}g=s._channelDomain;if(g&&X){k=s.split(g,'>');l=k.length;for(m=0;m<l;m++){q=s.split(k[m],'|');r=s.split(q[1],',');S=r.length;for(T=0;T<S;T++){Y=r[T];Y=Y.toLowerCase();i=j.indexOf(Y);if(i>-1)P=q[0]}}}g=s._channelParameter;if(g&&X){k=s.split(g,'>');l=k.length;for(m=0;m<l;m++){q=s.split(k[m],'|');r=s.split(q[1],',');S=r.length;for(T=0;T<S;T++){U=s.getQueryParam(r[T]);if(U)P=q[0]}}}g=s._channelPattern;if(g&&X){k=s.split(g,'>');l=k.length;for(m=0;m<l;m++){q=s.split(k[m],'|');r=s.split(q[1],',');S=r.length;for(T=0;T<S;T++){Y=r[T];Y=Y.toLowerCase();i=O.toLowerCase();H=i.indexOf(Y);if(H==0)P=q[0]}}}if(X)M=M?M:'n/a';p=X&&p?p:'';t=X&&t?t:'';N=X&&N?N:'';O=X&&O?O:'';u=X&&u?u:'';M=X&&M?M:'';P=X&&P?P:'';s._referrer=p;s._referringDomain=t;s._partner=N;s._campaignID=O;s._campaign=u;s._keywords=M;s._channel=P");
s.seList = "search.aol.com,search.aol.ca|query,q|AOL.com Search>ask.com,ask.co.uk|ask,q|Ask Jeeves>www.baidu.com|wd,word|Baidu>www.baidu.jp|wd,word|Baidu Japan>search.biglobe.ne.jp|q|Biglobe>daum.net,search.daum.net|q|Daum>Dictionary.com,Dictionary|term,query,q|Dictionary.com>excite.co.jp|search,s|Excite - Japan>search.fresheye.com|ord,kw|FreshEye>goo.ne.jp|MT|Goo (Jp.)>google.co,googlesyndication.com|q,as_q|Google>google.am|q,as_q|Google - Armenia>google.com.au|q,as_q|Google - Australia>google.com.bh|q,as_q|Google - Bahrain>google.be|q,as_q|Google - Belgium>google.com.bo|q,as_q|Google - Bolivia>google.com.br|q,as_q|Google - Brasil>google.bg|q,as_q|Google - Bulgaria>google.ca|q,as_q|Google - Canada>google.cn|q,as_q|Google - China>google.com.co|q,as_q|Google - Colombia>google.hr|q,as_q|Google - Croatia>google.dk|q,as_q|Google - Denmark>google.com.eg|q,as_q|Google - Egypt>google.com.sv|q,as_q|Google - El Salvador>google.fi|q,as_q|Google - Finland>google.fr|q,as_q|Google - France>google.de|q,as_q|Google - Germany>google.com.hk|q,as_q|Google - Hong Kong>google.hu|q,as_q|Google - Hungary>google.co.in|q,as_q|Google - India>google.co.id|q,as_q|Google - Indonesia>google.ie|q,as_q|Google - Ireland>google.im|q,as_q|Google - Isle of Man>google.it|q,as_q|Google - Italy>google.co.jp|q,as_q|Google - Japan>google.kz|q,as_q|Google - Kazakhstan>google.co.ke|q,as_q|Google - Kenya>google.co.kr|q,as_q|Google - Korea>google.com.my|q,as_q|Google - Malaysia>google.com.mx|q,as_q|Google - Mexico>google.nl|q,as_q|Google - Netherlands>google.co.nz|q,as_q|Google - New Zealand>google.com.ni|q,as_q|Google - Nicaragua>google.com.pe|q,as_q|Google - Peru>google.com.ph|q,as_q|Google - Philippines>google.pl|q,as_q|Google - Poland>google.pt|q,as_q|Google - Portugal>google.ro|q,as_q|Google - Romania>google.ru|q,as_q|Google - Russia>google.rw|q,as_q|Google - Rwanda>google.com.sg|q,as_q|Google - Singapore>google.sk|q,as_q|Google - Slovakia>google.si|q,as_q|Google - Slovenia>google.es|q,as_q|Google - Spain>google.se|q,as_q|Google - Sweden>google.ch|q,as_q|Google - Switzerland>google.com.tw|q,as_q|Google - Taiwan>google.co.th|q,as_q|Google - Thailand>google.tt|q,as_q|Google - Trinidad and Tobago>google.com.tr|q,as_q|Google - Turkey>google.ae|q,as_q|Google - United Arab Emirates>google.co.uk|q,as_q|Google - United Kingdom>google.com.vn|q,as_q|Google - Viet Nam>icqit.com|q|icq>infoseek.co.jp|qt|Infoseek - Japan>kakaku.com|query|Kakaku>search.livedoor.com|q|Livedoor.com>bing.com|q|Microsoft Bing>myway.com|searchfor|MyWay.com>search.nifty.com|q|Nifty>odn.excite.co.jp|search|ODN>overture.com|Keywords|Overture>search.ch|q|Search.ch>yahoo.com,search.yahoo.com|p|Yahoo!>au.yahoo.com,au.search.yahoo.com|p|Yahoo! - Australia>ca.yahoo.com,ca.search.yahoo.com|p|Yahoo! - Canada>hk.yahoo.com,hk.search.yahoo.com|p|Yahoo! - Hong Kong>yahoo.co.jp,search.yahoo.co.jp|p,va|Yahoo! - Japan>malaysia.yahoo.com,malaysia.search.yahoo.com|p|Yahoo! - Malaysia>sg.yahoo.com,sg.search.yahoo.com|p|Yahoo! - Singapore>es.yahoo.com,es.search.yahoo.com|p|Yahoo! - Spain>tw.yahoo.com,tw.search.yahoo.com|p|Yahoo! - Taiwan>uk.yahoo.com,uk.search.yahoo.com|p|Yahoo! - UK and Ireland>vn.yahoo.com,vn.search.yahoo.com|p|Yahoo! - Viet Nam>mobile.yahoo.co.jp|p|YahooJapan - Mobile>search.cnn.com|query|CNN Web Search>search.earthlink.net|q|Earthlink Search>search.comcast.net|q|Comcast Search>search.rr.com|qs|RoadRunner Search>optimum.net|q|Optimum Search>m.bing.com|Q|m.bing>search.mywebsearch.com|searchfor|mywebsearch>www.sogou.com|query|Sogou";
s.seList = s.seList + ">s.luna.tv|q|Luna.TV>so-net.ne.jp|query|So-net>search.jword.jp|name|JWord";
s.getPreviousValue = new Function("v", "c", "el", "var s=this,t=new Date,i,j,r='';t.setTime(t.getTime()+1800000);if(el){if(s.events){i=s.split(el,',');j=s.split(s.events,',');for(x in i){for(y in j){if(i[x]==j[y]){if(s.c_r(c)) r=s.c_r(c);v?s.c_w(c,v,t):s.c_w(c,'no value',t);return r}}}}}else{if(s.c_r(c)) r=s.c_r(c);v?s.c_w(c,v,t):s.c_w(c,'no value',t);return r}");
s.getCk = new Function("c", "var s=this,k=s.c_r(c);return k;");
s.setCk = new Function("c", "v", "e", "var s=this,a=new Date;e=e?e:0;a.setTime(a.getTime()+e*86400000);s.c_w(c,v,e?a:0);");
s.apl = new Function("l", "v", "d", "u", "var s=this,m=0;if(!l)l='';if(u){var i,n,a=s.split(l,d);for(i=0;i<a.length;i++){n=a[i];m=m||(u==1?(n==v):(n.toLowerCase()==v.toLowerCase()));}}if(!m)l=l?l+d+v:v;return l");
s.split = new Function("l", "d", "var i,x=0,a=new Array;while(l){i=l.indexOf(d);i=i>-1?i:l.length;a[x++]=l.substring(0,i);l=l.substring(i+d.length);}return a");
s.p_c = new Function("v", "c", "var x=v.indexOf('=');return c.toLowerCase()==v.substring(0,x<0?v.length:x).toLowerCase()?v:0");
s.repl = new Function("x", "o", "n", "var i=x.indexOf(o),l=n.length;while(x&&i>=0){x=x.substring(0,i)+n+x.substring(i+o.length);i=x.indexOf(o,i+l)}return x");
/*!************************* ActivityMap Module *************************/
function AppMeasurement_Module_ActivityMap(f){function g(a,d){var b,c,n;if(a&&d&&(b=e.c[d]||(e.c[d]=d.split(",")))){for(n=0;n<b.length&&(c=b[n++]);){if(-1<a.indexOf(c)){return null}}}p=1;return a}function q(a,d,b,c,e){var g,h;if(a.dataset&&(h=a.dataset[d])){g=h}else{if(a.getAttribute){if(h=a.getAttribute("data-"+b)){g=h}else{if(h=a.getAttribute(b)){g=h}}}}if(!g&&f.useForcedLinkTracking&&e&&(g="",d=a.onclick?""+a.onclick:"")){b=d.indexOf(c);var l,k;if(0<=b){for(b+=10;b<d.length&&0<="= \t\r\n".indexOf(d.charAt(b));){b++}if(b<d.length){h=b;for(l=k=0;h<d.length&&(";"!=d.charAt(h)||l);){l?d.charAt(h)!=l||k?k="\\"==d.charAt(h)?!k:0:l=0:(l=d.charAt(h),'"'!=l&&"'"!=l&&(l=0)),h++}if(d=d.substring(b,h)){a.e=new Function("s","var e;try{s.w."+c+"="+d+"}catch(e){}"),a.e(f)}}}}return g||e&&f.w[c]}function r(a,d,b){var c;return(c=e[d](a,b))&&(p?(p=0,c):g(k(c),e[d+"Exclusions"]))}function s(a,d,b){var c;if(a&&!(1===(c=a.nodeType)&&(c=a.nodeName)&&(c=c.toUpperCase())&&t[c])&&(1===a.nodeType&&(c=a.nodeValue)&&(d[d.length]=c),b.a||b.t||b.s||!a.getAttribute||((c=a.getAttribute("alt"))?b.a=c:(c=a.getAttribute("title"))?b.t=c:"IMG"==(""+a.nodeName).toUpperCase()&&(c=a.getAttribute("src")||a.src)&&(b.s=c)),(c=a.childNodes)&&c.length)){for(a=0;a<c.length;a++){s(c[a],d,b)}}}function k(a){if(null==a||void 0==a){return a}try{return a.replace(RegExp("^[\\s\\n\\f\\r\\t\t-\r \u00a0\u1680\u180e\u2000-\u200a\u2028\u2029\u205f\u3000\ufeff]+","mg"),"").replace(RegExp("[\\s\\n\\f\\r\\t\t-\r \u00a0\u1680\u180e\u2000-\u200a\u2028\u2029\u205f\u3000\ufeff]+$","mg"),"").replace(RegExp("[\\s\\n\\f\\r\\t\t-\r \u00a0\u1680\u180e\u2000-\u200a\u2028\u2029\u205f\u3000\ufeff]{1,}","mg")," ").substring(0,254)}catch(d){}}var e=this;e.s=f;var m=window;m.s_c_in||(m.s_c_il=[],m.s_c_in=0);e._il=m.s_c_il;e._in=m.s_c_in;e._il[e._in]=e;m.s_c_in++;e._c="s_m";e.c={};var p=0,t={SCRIPT:1,STYLE:1,LINK:1,CANVAS:1};e._g=function(){var a,d,b,c=f.contextData,e=f.linkObject;(a=f.pageName||f.pageURL)&&(d=r(e,"link",f.linkName))&&(b=r(e,"region"))&&(c["a.activitymap.page"]=a.substring(0,255),c["a.activitymap.link"]=128<d.length?d.substring(0,128):d,c["a.activitymap.region"]=127<b.length?b.substring(0,127):b,c["a.activitymap.pageIDType"]=f.pageName?1:0)};e.link=function(a,d){var b;if(d){b=g(k(d),e.linkExclusions)}else{if((b=a)&&!(b=q(a,"sObjectId","s-object-id","s_objectID",1))){var c,f;(f=g(k(a.innerText||a.textContent),e.linkExclusions))||(s(a,c=[],b={a:void 0,t:void 0,s:void 0}),(f=g(k(c.join(""))))||(f=g(k(b.a?b.a:b.t?b.t:b.s?b.s:void 0)))||!(c=(c=a.tagName)&&c.toUpperCase?c.toUpperCase():"")||("INPUT"==c||"SUBMIT"==c&&a.value?f=g(k(a.value)):"IMAGE"==c&&a.src&&(f=g(k(a.src)))));b=f}}return b};e.region=function(a){for(var d,b=e.regionIDAttribute||"id";a&&(a=a.parentNode);){if(d=q(a,b,b,b)){return d}if("BODY"==a.nodeName){return"BODY"}}}}
/*!************************* AppMeasurement for JavaScript version: 1.7.0 *************************/
function AppMeasurement(){var a=this;a.version="1.7.0";var k=window;k.s_c_in||(k.s_c_il=[],k.s_c_in=0);a._il=k.s_c_il;a._in=k.s_c_in;a._il[a._in]=a;k.s_c_in++;a._c="s_c";var q=k.AppMeasurement.Jb;q||(q=null);var r=k,n,t;try{for(n=r.parent,t=r.location;n&&n.location&&t&&""+n.location!=""+t&&r.location&&""+n.location!=""+r.location&&n.location.host==t.host;){r=n,n=r.parent}}catch(u){}a.yb=function(a){try{console.log(a)}catch(b){}};a.Ha=function(a){return""+parseInt(a)==""+a};a.replace=function(a,b,d){return !a||0>a.indexOf(b)?a:a.split(b).join(d)};a.escape=function(c){var b,d;if(!c){return c}c=encodeURIComponent(c);for(b=0;7>b;b++){d="+~!*()'".substring(b,b+1),0<=c.indexOf(d)&&(c=a.replace(c,d,"%"+d.charCodeAt(0).toString(16).toUpperCase()))}return c};a.unescape=function(c){if(!c){return c}c=0<=c.indexOf("+")?a.replace(c,"+"," "):c;try{return decodeURIComponent(c)}catch(b){}return unescape(c)};a.pb=function(){var c=k.location.hostname,b=a.fpCookieDomainPeriods,d;b||(b=a.cookieDomainPeriods);if(c&&!a.cookieDomain&&!/^[0-9.]+$/.test(c)&&(b=b?parseInt(b):2,b=2<b?b:2,d=c.lastIndexOf("."),0<=d)){for(;0<=d&&1<b;){d=c.lastIndexOf(".",d-1),b--}a.cookieDomain=0<d?c.substring(d):c}return a.cookieDomain};a.c_r=a.cookieRead=function(c){c=a.escape(c);var b=" "+a.d.cookie,d=b.indexOf(" "+c+"="),f=0>d?d:b.indexOf(";",d);c=0>d?"":a.unescape(b.substring(d+2+c.length,0>f?b.length:f));return"[[B]]"!=c?c:""};a.c_w=a.cookieWrite=function(c,b,d){var f=a.pb(),e=a.cookieLifetime,g;b=""+b;e=e?(""+e).toUpperCase():"";d&&"SESSION"!=e&&"NONE"!=e&&((g=""!=b?parseInt(e?e:0):-60)?(d=new Date,d.setTime(d.getTime()+1000*g)):1==d&&(d=new Date,g=d.getYear(),d.setYear(g+5+(1900>g?1900:0))));return c&&"NONE"!=e?(a.d.cookie=a.escape(c)+"="+a.escape(""!=b?b:"[[B]]")+"; path=/;"+(d&&"SESSION"!=e?" expires="+d.toGMTString()+";":"")+(f?" domain="+f+";":""),a.cookieRead(c)==b):0};a.K=[];a.ha=function(c,b,d){if(a.Aa){return 0}a.maxDelay||(a.maxDelay=250);var f=0,e=(new Date).getTime()+a.maxDelay,g=a.d.visibilityState,m=["webkitvisibilitychange","visibilitychange"];g||(g=a.d.webkitVisibilityState);if(g&&"prerender"==g){if(!a.ia){for(a.ia=1,d=0;d<m.length;d++){a.d.addEventListener(m[d],function(){var b=a.d.visibilityState;b||(b=a.d.webkitVisibilityState);"visible"==b&&(a.ia=0,a.delayReady())})}}f=1;e=0}else{d||a.p("_d")&&(f=1)}f&&(a.K.push({m:c,a:b,t:e}),a.ia||setTimeout(a.delayReady,a.maxDelay));return f};a.delayReady=function(){var c=(new Date).getTime(),b=0,d;for(a.p("_d")?b=1:a.va();0<a.K.length;){d=a.K.shift();if(b&&!d.t&&d.t>c){a.K.unshift(d);setTimeout(a.delayReady,parseInt(a.maxDelay/2));break}a.Aa=1;a[d.m].apply(a,d.a);a.Aa=0}};a.setAccount=a.sa=function(c){var b,d;if(!a.ha("setAccount",arguments)){if(a.account=c,a.allAccounts){for(b=a.allAccounts.concat(c.split(",")),a.allAccounts=[],b.sort(),d=0;d<b.length;d++){0!=d&&b[d-1]==b[d]||a.allAccounts.push(b[d])}}else{a.allAccounts=c.split(",")}}};a.foreachVar=function(c,b){var d,f,e,g,m="";e=f="";if(a.lightProfileID){d=a.O,(m=a.lightTrackVars)&&(m=","+m+","+a.ma.join(",")+",")}else{d=a.g;if(a.pe||a.linkType){m=a.linkTrackVars,f=a.linkTrackEvents,a.pe&&(e=a.pe.substring(0,1).toUpperCase()+a.pe.substring(1),a[e]&&(m=a[e].Hb,f=a[e].Gb))}m&&(m=","+m+","+a.G.join(",")+",");f&&m&&(m+=",events,")}b&&(b=","+b+",");for(f=0;f<d.length;f++){e=d[f],(g=a[e])&&(!m||0<=m.indexOf(","+e+","))&&(!b||0<=b.indexOf(","+e+","))&&c(e,g)}};a.r=function(c,b,d,f,e){var g="",m,p,k,w,n=0;"contextData"==c&&(c="c");if(b){for(m in b){if(!(Object.prototype[m]||e&&m.substring(0,e.length)!=e)&&b[m]&&(!d||0<=d.indexOf(","+(f?f+".":"")+m+","))){k=!1;if(n){for(p=0;p<n.length;p++){m.substring(0,n[p].length)==n[p]&&(k=!0)}}if(!k&&(""==g&&(g+="&"+c+"."),p=b[m],e&&(m=m.substring(e.length)),0<m.length)){if(k=m.indexOf("."),0<k){p=m.substring(0,k),k=(e?e:"")+p+".",n||(n=[]),n.push(k),g+=a.r(p,b,d,f,k)}else{if("boolean"==typeof p&&(p=p?"true":"false"),p){if("retrieveLightData"==f&&0>e.indexOf(".contextData.")){switch(k=m.substring(0,4),w=m.substring(4),m){case"transactionID":m="xact";break;case"channel":m="ch";break;case"campaign":m="v0";break;default:a.Ha(w)&&("prop"==k?m="c"+w:"eVar"==k?m="v"+w:"list"==k?m="l"+w:"hier"==k&&(m="h"+w,p=p.substring(0,255)))}}g+="&"+a.escape(m)+"="+a.escape(p)}}}}}""!=g&&(g+="&."+c)}return g};a.usePostbacks=0;a.sb=function(){var c="",b,d,f,e,g,m,p,k,n="",r="",s=e="";if(a.lightProfileID){b=a.O,(n=a.lightTrackVars)&&(n=","+n+","+a.ma.join(",")+",")}else{b=a.g;if(a.pe||a.linkType){n=a.linkTrackVars,r=a.linkTrackEvents,a.pe&&(e=a.pe.substring(0,1).toUpperCase()+a.pe.substring(1),a[e]&&(n=a[e].Hb,r=a[e].Gb))}n&&(n=","+n+","+a.G.join(",")+",");r&&(r=","+r+",",n&&(n+=",events,"));a.events2&&(s+=(""!=s?",":"")+a.events2)}if(a.visitor&&1.5<=parseFloat(a.visitor.version)&&a.visitor.getCustomerIDs){e=q;if(g=a.visitor.getCustomerIDs()){for(d in g){Object.prototype[d]||(f=g[d],e||(e={}),f.id&&(e[d+".id"]=f.id),f.authState&&(e[d+".as"]=f.authState))}}e&&(c+=a.r("cid",e))}a.AudienceManagement&&a.AudienceManagement.isReady()&&(c+=a.r("d",a.AudienceManagement.getEventCallConfigParams()));for(d=0;d<b.length;d++){e=b[d];g=a[e];f=e.substring(0,4);m=e.substring(4);!g&&"events"==e&&s&&(g=s,s="");if(g&&(!n||0<=n.indexOf(","+e+","))){switch(e){case"supplementalDataID":e="sdid";break;case"timestamp":e="ts";break;case"dynamicVariablePrefix":e="D";break;case"visitorID":e="vid";break;case"marketingCloudVisitorID":e="mid";break;case"analyticsVisitorID":e="aid";break;case"audienceManagerLocationHint":e="aamlh";break;case"audienceManagerBlob":e="aamb";break;case"authState":e="as";break;case"pageURL":e="g";255<g.length&&(a.pageURLRest=g.substring(255),g=g.substring(0,255));break;case"pageURLRest":e="-g";break;case"referrer":e="r";break;case"vmk":case"visitorMigrationKey":e="vmt";break;case"visitorMigrationServer":e="vmf";a.ssl&&a.visitorMigrationServerSecure&&(g="");break;case"visitorMigrationServerSecure":e="vmf";!a.ssl&&a.visitorMigrationServer&&(g="");break;case"charSet":e="ce";break;case"visitorNamespace":e="ns";break;case"cookieDomainPeriods":e="cdp";break;case"cookieLifetime":e="cl";break;case"variableProvider":e="vvp";break;case"currencyCode":e="cc";break;case"channel":e="ch";break;case"transactionID":e="xact";break;case"campaign":e="v0";break;case"latitude":e="lat";break;case"longitude":e="lon";break;case"resolution":e="s";break;case"colorDepth":e="c";break;case"javascriptVersion":e="j";break;case"javaEnabled":e="v";break;case"cookiesEnabled":e="k";break;case"browserWidth":e="bw";break;case"browserHeight":e="bh";break;case"connectionType":e="ct";break;case"homepage":e="hp";break;case"events":s&&(g+=(""!=g?",":"")+s);if(r){for(m=g.split(","),g="",f=0;f<m.length;f++){p=m[f],k=p.indexOf("="),0<=k&&(p=p.substring(0,k)),k=p.indexOf(":"),0<=k&&(p=p.substring(0,k)),0<=r.indexOf(","+p+",")&&(g+=(g?",":"")+m[f])}}break;case"events2":g="";break;case"contextData":c+=a.r("c",a[e],n,e);g="";break;case"lightProfileID":e="mtp";break;case"lightStoreForSeconds":e="mtss";a.lightProfileID||(g="");break;case"lightIncrementBy":e="mti";a.lightProfileID||(g="");break;case"retrieveLightProfiles":e="mtsr";break;case"deleteLightProfiles":e="mtsd";break;case"retrieveLightData":a.retrieveLightProfiles&&(c+=a.r("mts",a[e],n,e));g="";break;default:a.Ha(m)&&("prop"==f?e="c"+m:"eVar"==f?e="v"+m:"list"==f?e="l"+m:"hier"==f&&(e="h"+m,g=g.substring(0,255)))}g&&(c+="&"+e+"="+("pev"!=e.substring(0,3)?a.escape(g):g))}"pev3"==e&&a.e&&(c+=a.e)}return c};a.D=function(a){var b=a.tagName;if("undefined"!=""+a.Mb||"undefined"!=""+a.Cb&&"HTML"!=(""+a.Cb).toUpperCase()){return""}b=b&&b.toUpperCase?b.toUpperCase():"";"SHAPE"==b&&(b="");b&&(("INPUT"==b||"BUTTON"==b)&&a.type&&a.type.toUpperCase?b=a.type.toUpperCase():!b&&a.href&&(b="A"));return b};a.Da=function(a){var b=a.href?a.href:"",d,f,e;d=b.indexOf(":");f=b.indexOf("?");e=b.indexOf("/");b&&(0>d||0<=f&&d>f||0<=e&&d>e)&&(f=a.protocol&&1<a.protocol.length?a.protocol:l.protocol?l.protocol:"",d=l.pathname.lastIndexOf("/"),b=(f?f+"//":"")+(a.host?a.host:l.host?l.host:"")+("/"!=h.substring(0,1)?l.pathname.substring(0,0>d?0:d)+"/":"")+b);return b};a.L=function(c){var b=a.D(c),d,f,e="",g=0;return b&&(d=c.protocol,f=c.onclick,!c.href||"A"!=b&&"AREA"!=b||f&&d&&!(0>d.toLowerCase().indexOf("javascript"))?f?(e=a.replace(a.replace(a.replace(a.replace(""+f,"\r",""),"\n",""),"\t","")," ",""),g=2):"INPUT"==b||"SUBMIT"==b?(c.value?e=c.value:c.innerText?e=c.innerText:c.textContent&&(e=c.textContent),g=3):"IMAGE"==b&&c.src&&(e=c.src):e=a.Da(c),e)?{id:e.substring(0,100),type:g}:0};a.Kb=function(c){for(var b=a.D(c),d=a.L(c);c&&!d&&"BODY"!=b;){if(c=c.parentElement?c.parentElement:c.parentNode){b=a.D(c),d=a.L(c)}}d&&"BODY"!=b||(c=0);c&&(b=c.onclick?""+c.onclick:"",0<=b.indexOf(".tl(")||0<=b.indexOf(".trackLink("))&&(c=0);return c};a.Bb=function(){var c,b,d=a.linkObject,f=a.linkType,e=a.linkURL,g,m;a.na=1;d||(a.na=0,d=a.clickObject);if(d){c=a.D(d);for(b=a.L(d);d&&!b&&"BODY"!=c;){if(d=d.parentElement?d.parentElement:d.parentNode){c=a.D(d),b=a.L(d)}}b&&"BODY"!=c||(d=0);if(d&&!a.linkObject){var p=d.onclick?""+d.onclick:"";if(0<=p.indexOf(".tl(")||0<=p.indexOf(".trackLink(")){d=0}}}else{a.na=1}!e&&d&&(e=a.Da(d));e&&!a.linkLeaveQueryString&&(g=e.indexOf("?"),0<=g&&(e=e.substring(0,g)));if(!f&&e){var n=0,r=0,q;if(a.trackDownloadLinks&&a.linkDownloadFileTypes){for(p=e.toLowerCase(),g=p.indexOf("?"),m=p.indexOf("#"),0<=g?0<=m&&m<g&&(g=m):g=m,0<=g&&(p=p.substring(0,g)),g=a.linkDownloadFileTypes.toLowerCase().split(","),m=0;m<g.length;m++){(q=g[m])&&p.substring(p.length-(q.length+1))=="."+q&&(f="d")}}if(a.trackExternalLinks&&!f&&(p=e.toLowerCase(),a.Ga(p)&&(a.linkInternalFilters||(a.linkInternalFilters=k.location.hostname),g=0,a.linkExternalFilters?(g=a.linkExternalFilters.toLowerCase().split(","),n=1):a.linkInternalFilters&&(g=a.linkInternalFilters.toLowerCase().split(",")),g))){for(m=0;m<g.length;m++){q=g[m],0<=p.indexOf(q)&&(r=1)}r?n&&(f="e"):n||(f="e")}}a.linkObject=d;a.linkURL=e;a.linkType=f;if(a.trackClickMap||a.trackInlineStats){a.e="",d&&(f=a.pageName,e=1,d=d.sourceIndex,f||(f=a.pageURL,e=0),k.s_objectID&&(b.id=k.s_objectID,d=b.type=1),f&&b&&b.id&&c&&(a.e="&pid="+a.escape(f.substring(0,255))+(e?"&pidt="+e:"")+"&oid="+a.escape(b.id.substring(0,100))+(b.type?"&oidt="+b.type:"")+"&ot="+c+(d?"&oi="+d:"")))}};a.tb=function(){var c=a.na,b=a.linkType,d=a.linkURL,f=a.linkName;b&&(d||f)&&(b=b.toLowerCase(),"d"!=b&&"e"!=b&&(b="o"),a.pe="lnk_"+b,a.pev1=d?a.escape(d):"",a.pev2=f?a.escape(f):"",c=1);a.abort&&(c=0);if(a.trackClickMap||a.trackInlineStats||a.ActivityMap){var b={},d=0,e=a.cookieRead("s_sq"),g=e?e.split("&"):0,m,p,k,e=0;if(g){for(m=0;m<g.length;m++){p=g[m].split("="),f=a.unescape(p[0]).split(","),p=a.unescape(p[1]),b[p]=f}}f=a.account.split(",");m={};for(k in a.contextData){k&&!Object.prototype[k]&&"a.activitymap."==k.substring(0,14)&&(m[k]=a.contextData[k],a.contextData[k]="")}a.e=a.r("c",m)+(a.e?a.e:"");if(c||a.e){c&&!a.e&&(e=1);for(p in b){if(!Object.prototype[p]){for(k=0;k<f.length;k++){for(e&&(g=b[p].join(","),g==a.account&&(a.e+=("&"!=p.charAt(0)?"&":"")+p,b[p]=[],d=1)),m=0;m<b[p].length;m++){g=b[p][m],g==f[k]&&(e&&(a.e+="&u="+a.escape(g)+("&"!=p.charAt(0)?"&":"")+p+"&u=0"),b[p].splice(m,1),d=1)}}}}c||(d=1);if(d){e="";m=2;!c&&a.e&&(e=a.escape(f.join(","))+"="+a.escape(a.e),m=1);for(p in b){!Object.prototype[p]&&0<m&&0<b[p].length&&(e+=(e?"&":"")+a.escape(b[p].join(","))+"="+a.escape(p),m--)}a.cookieWrite("s_sq",e)}}}return c};a.ub=function(){if(!a.Fb){var c=new Date,b=r.location,d,f,e=f=d="",g="",m="",k="1.2",n=a.cookieWrite("s_cc","true",0)?"Y":"N",q="",s="";if(c.setUTCDate&&(k="1.3",(0).toPrecision&&(k="1.5",c=[],c.forEach))){k="1.6";f=0;d={};try{f=new Iterator(d),f.next&&(k="1.7",c.reduce&&(k="1.8",k.trim&&(k="1.8.1",Date.parse&&(k="1.8.2",Object.create&&(k="1.8.5")))))}catch(t){}}d=screen.width+"x"+screen.height;e=navigator.javaEnabled()?"Y":"N";f=screen.pixelDepth?screen.pixelDepth:screen.colorDepth;g=a.w.innerWidth?a.w.innerWidth:a.d.documentElement.offsetWidth;m=a.w.innerHeight?a.w.innerHeight:a.d.documentElement.offsetHeight;try{a.b.addBehavior("#default#homePage"),q=a.b.Lb(b)?"Y":"N"}catch(u){}try{a.b.addBehavior("#default#clientCaps"),s=a.b.connectionType}catch(x){}a.resolution=d;a.colorDepth=f;a.javascriptVersion=k;a.javaEnabled=e;a.cookiesEnabled=n;a.browserWidth=g;a.browserHeight=m;a.connectionType=s;a.homepage=q;a.Fb=1}};a.P={};a.loadModule=function(c,b){var d=a.P[c];if(!d){d=k["AppMeasurement_Module_"+c]?new k["AppMeasurement_Module_"+c](a):{};a.P[c]=a[c]=d;d.Xa=function(){return d.ab};d.bb=function(b){if(d.ab=b){a[c+"_onLoad"]=b,a.ha(c+"_onLoad",[a,d],1)||b(a,d)}};try{Object.defineProperty?Object.defineProperty(d,"onLoad",{get:d.Xa,set:d.bb}):d._olc=1}catch(f){d._olc=1}}b&&(a[c+"_onLoad"]=b,a.ha(c+"_onLoad",[a,d],1)||b(a,d))};a.p=function(c){var b,d;for(b in a.P){if(!Object.prototype[b]&&(d=a.P[b])&&(d._olc&&d.onLoad&&(d._olc=0,d.onLoad(a,d)),d[c]&&d[c]())){return 1}}return 0};a.wb=function(){var c=Math.floor(10000000000000*Math.random()),b=a.visitorSampling,d=a.visitorSamplingGroup,d="s_vsn_"+(a.visitorNamespace?a.visitorNamespace:a.account)+(d?"_"+d:""),f=a.cookieRead(d);if(b){f&&(f=parseInt(f));if(!f){if(!a.cookieWrite(d,c)){return 0}f=c}if(f%10000>v){return 0}}return 1};a.Q=function(c,b){var d,f,e,g,m,k;for(d=0;2>d;d++){for(f=0<d?a.wa:a.g,e=0;e<f.length;e++){if(g=f[e],(m=c[g])||c["!"+g]){if(!b&&("contextData"==g||"retrieveLightData"==g)&&a[g]){for(k in a[g]){m[k]||(m[k]=a[g][k])}}a[g]=m}}}};a.Qa=function(c,b){var d,f,e,g;for(d=0;2>d;d++){for(f=0<d?a.wa:a.g,e=0;e<f.length;e++){g=f[e],c[g]=a[g],b||c[g]||(c["!"+g]=1)}}};a.ob=function(a){var b,d,f,e,g,k=0,p,n="",q="";if(a&&255<a.length&&(b=""+a,d=b.indexOf("?"),0<d&&(p=b.substring(d+1),b=b.substring(0,d),e=b.toLowerCase(),f=0,"http://"==e.substring(0,7)?f+=7:"https://"==e.substring(0,8)&&(f+=8),d=e.indexOf("/",f),0<d&&(e=e.substring(f,d),g=b.substring(d),b=b.substring(0,d),0<=e.indexOf("google")?k=",q,ie,start,search_key,word,kw,cd,":0<=e.indexOf("yahoo.co")&&(k=",p,ei,"),k&&p)))){if((a=p.split("&"))&&1<a.length){for(f=0;f<a.length;f++){e=a[f],d=e.indexOf("="),0<d&&0<=k.indexOf(","+e.substring(0,d)+",")?n+=(n?"&":"")+e:q+=(q?"&":"")+e}n&&q?p=n+"&"+q:q=""}d=253-(p.length-q.length)-b.length;a=b+(0<d?g.substring(0,d):"")+"?"+p}return a};a.Wa=function(c){var b=a.d.visibilityState,d=["webkitvisibilitychange","visibilitychange"];b||(b=a.d.webkitVisibilityState);if(b&&"prerender"==b){if(c){for(b=0;b<d.length;b++){a.d.addEventListener(d[b],function(){var b=a.d.visibilityState;b||(b=a.d.webkitVisibilityState);"visible"==b&&c()})}}return !1}return !0};a.da=!1;a.I=!1;a.eb=function(){a.I=!0;a.j()};a.ba=!1;a.U=!1;a.$a=function(c){a.marketingCloudVisitorID=c;a.U=!0;a.j()};a.ea=!1;a.V=!1;a.fb=function(c){a.visitorOptedOut=c;a.V=!0;a.j()};a.Y=!1;a.R=!1;a.Sa=function(c){a.analyticsVisitorID=c;a.R=!0;a.j()};a.aa=!1;a.T=!1;a.Ua=function(c){a.audienceManagerLocationHint=c;a.T=!0;a.j()};a.Z=!1;a.S=!1;a.Ta=function(c){a.audienceManagerBlob=c;a.S=!0;a.j()};a.Va=function(c){a.maxDelay||(a.maxDelay=250);return a.p("_d")?(c&&setTimeout(function(){c()},a.maxDelay),!1):!0};a.ca=!1;a.H=!1;a.va=function(){a.H=!0;a.j()};a.isReadyToTrack=function(){var c=!0,b=a.visitor,d,f,e;a.da||a.I||(a.Wa(a.eb)?a.I=!0:a.da=!0);if(a.da&&!a.I){return !1}b&&b.isAllowed()&&(a.ba||a.marketingCloudVisitorID||!b.getMarketingCloudVisitorID||(a.ba=!0,a.marketingCloudVisitorID=b.getMarketingCloudVisitorID([a,a.$a]),a.marketingCloudVisitorID&&(a.U=!0)),a.ea||a.visitorOptedOut||!b.isOptedOut||(a.ea=!0,a.visitorOptedOut=b.isOptedOut([a,a.fb]),a.visitorOptedOut!=q&&(a.V=!0)),a.Y||a.analyticsVisitorID||!b.getAnalyticsVisitorID||(a.Y=!0,a.analyticsVisitorID=b.getAnalyticsVisitorID([a,a.Sa]),a.analyticsVisitorID&&(a.R=!0)),a.aa||a.audienceManagerLocationHint||!b.getAudienceManagerLocationHint||(a.aa=!0,a.audienceManagerLocationHint=b.getAudienceManagerLocationHint([a,a.Ua]),a.audienceManagerLocationHint&&(a.T=!0)),a.Z||a.audienceManagerBlob||!b.getAudienceManagerBlob||(a.Z=!0,a.audienceManagerBlob=b.getAudienceManagerBlob([a,a.Ta]),a.audienceManagerBlob&&(a.S=!0)),c=a.ba&&!a.U&&!a.marketingCloudVisitorID,b=a.Y&&!a.R&&!a.analyticsVisitorID,d=a.aa&&!a.T&&!a.audienceManagerLocationHint,f=a.Z&&!a.S&&!a.audienceManagerBlob,e=a.ea&&!a.V,c=c||b||d||f||e?!1:!0);a.ca||a.H||(a.Va(a.va)?a.H=!0:a.ca=!0);a.ca&&!a.H&&(c=!1);return c};a.o=q;a.u=0;a.callbackWhenReadyToTrack=function(c,b,d){var f;f={};f.jb=c;f.ib=b;f.gb=d;a.o==q&&(a.o=[]);a.o.push(f);0==a.u&&(a.u=setInterval(a.j,100))};a.j=function(){var c;if(a.isReadyToTrack()&&(a.cb(),a.o!=q)){for(;0<a.o.length;){c=a.o.shift(),c.ib.apply(c.jb,c.gb)}}};a.cb=function(){a.u&&(clearInterval(a.u),a.u=0)};a.Ya=function(c){var b,d,f=q,e=q;if(!a.isReadyToTrack()){b=[];if(c!=q){for(d in f={},c){f[d]=c[d]}}e={};a.Qa(e,!0);b.push(f);b.push(e);a.callbackWhenReadyToTrack(a,a.track,b);return !0}return !1};a.qb=function(){var c=a.cookieRead("s_fid"),b="",d="",f;f=8;var e=4;if(!c||0>c.indexOf("-")){for(c=0;16>c;c++){f=Math.floor(Math.random()*f),b+="0123456789ABCDEF".substring(f,f+1),f=Math.floor(Math.random()*e),d+="0123456789ABCDEF".substring(f,f+1),f=e=16}c=b+"-"+d}a.cookieWrite("s_fid",c,1)||(c=0);return c};a.t=a.track=function(c,b){var d,f=new Date,e="s"+Math.floor(f.getTime()/10800000)%10+Math.floor(10000000000000*Math.random()),g=f.getYear(),g="t="+a.escape(f.getDate()+"/"+f.getMonth()+"/"+(1900>g?g+1900:g)+" "+f.getHours()+":"+f.getMinutes()+":"+f.getSeconds()+" "+f.getDay()+" "+f.getTimezoneOffset());a.visitor&&(a.visitor.getAuthState&&(a.authState=a.visitor.getAuthState()),!a.supplementalDataID&&a.visitor.getSupplementalDataID&&(a.supplementalDataID=a.visitor.getSupplementalDataID("AppMeasurement:"+a._in,a.expectSupplementalData?!1:!0)));a.p("_s");a.Ya(c)||(b&&a.Q(b),c&&(d={},a.Qa(d,0),a.Q(c)),a.wb()&&!a.visitorOptedOut&&(a.analyticsVisitorID||a.marketingCloudVisitorID||(a.fid=a.qb()),a.Bb(),a.usePlugins&&a.doPlugins&&a.doPlugins(a),a.account&&(a.abort||(a.trackOffline&&!a.timestamp&&(a.timestamp=Math.floor(f.getTime()/1000)),f=k.location,a.pageURL||(a.pageURL=f.href?f.href:f),a.referrer||a.Ra||(a.referrer=r.document.referrer),a.Ra=1,a.referrer=a.ob(a.referrer),a.p("_g")),a.tb()&&!a.abort&&(a.ub(),g+=a.sb(),a.Ab(e,g),a.p("_t"),a.referrer=""))),c&&a.Q(d,1));a.abort=a.supplementalDataID=a.timestamp=a.pageURLRest=a.linkObject=a.clickObject=a.linkURL=a.linkName=a.linkType=k.s_objectID=a.pe=a.pev1=a.pev2=a.pev3=a.e=a.lightProfileID=0};a.tl=a.trackLink=function(c,b,d,f,e){a.linkObject=c;a.linkType=b;a.linkName=d;e&&(a.l=c,a.A=e);return a.track(f)};a.trackLight=function(c,b,d,f){a.lightProfileID=c;a.lightStoreForSeconds=b;a.lightIncrementBy=d;return a.track(f)};a.clearVars=function(){var c,b;for(c=0;c<a.g.length;c++){if(b=a.g[c],"prop"==b.substring(0,4)||"eVar"==b.substring(0,4)||"hier"==b.substring(0,4)||"list"==b.substring(0,4)||"channel"==b||"events"==b||"eventList"==b||"products"==b||"productList"==b||"purchaseID"==b||"transactionID"==b||"state"==b||"zip"==b||"campaign"==b){a[b]=void 0}}};a.tagContainerMarker="";a.Ab=function(c,b){var d,f=a.trackingServer;d="";var e=a.dc,g="sc.",k=a.visitorNamespace;f?a.trackingServerSecure&&a.ssl&&(f=a.trackingServerSecure):(k||(k=a.account,f=k.indexOf(","),0<=f&&(k=k.substring(0,f)),k=k.replace(/[^A-Za-z0-9]/g,"")),d||(d="2o7.net"),e=e?(""+e).toLowerCase():"d1","2o7.net"==d&&("d1"==e?e="112":"d2"==e&&(e="122"),g=""),f=k+"."+e+"."+g+d);d=a.ssl?"https://":"http://";e=a.AudienceManagement&&a.AudienceManagement.isReady()||0!=a.usePostbacks;d+=f+"/b/ss/"+a.account+"/"+(a.mobile?"5.":"")+(e?"10":"1")+"/JS-"+a.version+(a.Eb?"T":"")+(a.tagContainerMarker?"-"+a.tagContainerMarker:"")+"/"+c+"?AQB=1&ndh=1&pf=1&"+(e?"callback=s_c_il["+a._in+"].doPostbacks&et=1&":"")+b+"&AQE=1";a.mb(d);a.ja()};a.Pa=/{(%?)(.*?)(%?)}/;a.Ib=RegExp(a.Pa.source,"g");a.nb=function(c){if("object"==typeof c.dests){for(var b=0;b<c.dests.length;++b){if(o=c.dests[b],"string"==typeof o.c&&"aa."==o.id.substr(0,3)){for(var d=o.c.match(a.Ib),b=0;b<d.length;++b){match=d[b];var f=match.match(a.Pa),e="";"%"==f[1]&&"timezone_offset"==f[2]?e=(new Date).getTimezoneOffset():"%"==f[1]&&"timestampz"==f[2]&&(e=a.rb());o.c=o.c.replace(match,a.escape(e))}}}}};a.rb=function(){var c=new Date,b=new Date(60000*Math.abs(c.getTimezoneOffset()));return a.k(4,c.getFullYear())+"-"+a.k(2,c.getMonth()+1)+"-"+a.k(2,c.getDate())+"T"+a.k(2,c.getHours())+":"+a.k(2,c.getMinutes())+":"+a.k(2,c.getSeconds())+(0<c.getTimezoneOffset()?"-":"+")+a.k(2,b.getUTCHours())+":"+a.k(2,b.getUTCMinutes())};a.k=function(a,b){return(Array(a+1).join(0)+b).slice(-a)};a.ra={};a.doPostbacks=function(c){if("object"==typeof c){if(a.nb(c),"object"==typeof a.AudienceManagement&&"function"==typeof a.AudienceManagement.isReady&&a.AudienceManagement.isReady()&&"function"==typeof a.AudienceManagement.passData){a.AudienceManagement.passData(c)}else{if("object"==typeof c&&"object"==typeof c.dests){for(var b=0;b<c.dests.length;++b){dest=c.dests[b],"object"==typeof dest&&"string"==typeof dest.c&&"string"==typeof dest.id&&"aa."==dest.id.substr(0,3)&&(a.ra[dest.id]=new Image,a.ra[dest.id].alt="",a.ra[dest.id].src=dest.c)}}}}};a.mb=function(c){a.i||a.vb();a.i.push(c);a.la=a.C();a.Na()};a.vb=function(){a.i=a.xb();a.i||(a.i=[])};a.xb=function(){var c,b;if(a.qa()){try{(b=k.localStorage.getItem(a.oa()))&&(c=k.JSON.parse(b))}catch(d){}return c}};a.qa=function(){var c=!0;a.trackOffline&&a.offlineFilename&&k.localStorage&&k.JSON||(c=!1);return c};a.Ea=function(){var c=0;a.i&&(c=a.i.length);a.q&&c++;return c};a.ja=function(){if(a.q&&(a.B&&a.B.complete&&a.B.F&&a.B.ua(),a.q)){return}a.Fa=q;if(a.pa){a.la>a.N&&a.La(a.i),a.ta(500)}else{var c=a.hb();if(0<c){a.ta(c)}else{if(c=a.Ba()){a.q=1,a.zb(c),a.Db(c)}}}};a.ta=function(c){a.Fa||(c||(c=0),a.Fa=setTimeout(a.ja,c))};a.hb=function(){var c;if(!a.trackOffline||0>=a.offlineThrottleDelay){return 0}c=a.C()-a.Ka;return a.offlineThrottleDelay<c?0:a.offlineThrottleDelay-c};a.Ba=function(){if(0<a.i.length){return a.i.shift()}};a.zb=function(c){if(a.debugTracking){var b="AppMeasurement Debug: "+c;c=c.split("&");var d;for(d=0;d<c.length;d++){b+="\n\t"+a.unescape(c[d])}a.yb(b)}};a.Za=function(){return a.marketingCloudVisitorID||a.analyticsVisitorID};a.X=!1;var s;try{s=JSON.parse('{"x":"y"}')}catch(x){s=null}s&&"y"==s.x?(a.X=!0,a.W=function(a){return JSON.parse(a)}):k.$&&k.$.parseJSON?(a.W=function(a){return k.$.parseJSON(a)},a.X=!0):a.W=function(){return null};a.Db=function(c){var b,d,f;a.Za()&&2047<c.length&&("undefined"!=typeof XMLHttpRequest&&(b=new XMLHttpRequest,"withCredentials" in b?d=1:b=0),b||"undefined"==typeof XDomainRequest||(b=new XDomainRequest,d=2),b&&(a.AudienceManagement&&a.AudienceManagement.isReady()||0!=a.usePostbacks)&&(a.X?b.xa=!0:b=0));!b&&a.Oa&&(c=c.substring(0,2047));!b&&a.d.createElement&&(0!=a.usePostbacks||a.AudienceManagement&&a.AudienceManagement.isReady())&&(b=a.d.createElement("SCRIPT"))&&"async" in b&&((f=(f=a.d.getElementsByTagName("HEAD"))&&f[0]?f[0]:a.d.body)?(b.type="text/javascript",b.setAttribute("async","async"),d=3):b=0);b||(b=new Image,b.alt="",b.abort||"undefined"===typeof k.InstallTrigger||(b.abort=function(){b.src=q}));b.za=function(){try{b.F&&(clearTimeout(b.F),b.F=0)}catch(a){}};b.onload=b.ua=function(){b.za();a.lb();a.fa();a.q=0;a.ja();if(b.xa){b.xa=!1;try{a.doPostbacks(a.W(b.responseText))}catch(c){}}};b.onabort=b.onerror=b.Ca=function(){b.za();(a.trackOffline||a.pa)&&a.q&&a.i.unshift(a.kb);a.q=0;a.la>a.N&&a.La(a.i);a.fa();a.ta(500)};b.onreadystatechange=function(){4==b.readyState&&(200==b.status?b.ua():b.Ca())};a.Ka=a.C();if(1==d||2==d){var e=c.indexOf("?");f=c.substring(0,e);e=c.substring(e+1);e=e.replace(/&callback=[a-zA-Z0-9_.\[\]]+/,"");1==d?(b.open("POST",f,!0),b.send(e)):2==d&&(b.open("POST",f),b.send(e))}else{if(b.src=c,3==d){if(a.Ia){try{f.removeChild(a.Ia)}catch(g){}}f.firstChild?f.insertBefore(b,f.firstChild):f.appendChild(b);a.Ia=a.B}}b.F=setTimeout(function(){b.F&&(b.complete?b.ua():(a.trackOffline&&b.abort&&b.abort(),b.Ca()))},5000);a.kb=c;a.B=k["s_i_"+a.replace(a.account,",","_")]=b;if(a.useForcedLinkTracking&&a.J||a.A){a.forcedLinkTrackingTimeout||(a.forcedLinkTrackingTimeout=250),a.ga=setTimeout(a.fa,a.forcedLinkTrackingTimeout)}};a.lb=function(){if(a.qa()&&!(a.Ja>a.N)){try{k.localStorage.removeItem(a.oa()),a.Ja=a.C()}catch(c){}}};a.La=function(c){if(a.qa()){a.Na();try{k.localStorage.setItem(a.oa(),k.JSON.stringify(c)),a.N=a.C()}catch(b){}}};a.Na=function(){if(a.trackOffline){if(!a.offlineLimit||0>=a.offlineLimit){a.offlineLimit=10}for(;a.i.length>a.offlineLimit;){a.Ba()}}};a.forceOffline=function(){a.pa=!0};a.forceOnline=function(){a.pa=!1};a.oa=function(){return a.offlineFilename+"-"+a.visitorNamespace+a.account};a.C=function(){return(new Date).getTime()};a.Ga=function(a){a=a.toLowerCase();return 0!=a.indexOf("#")&&0!=a.indexOf("about:")&&0!=a.indexOf("opera:")&&0!=a.indexOf("javascript:")?!0:!1};a.setTagContainer=function(c){var b,d,f;a.Eb=c;for(b=0;b<a._il.length;b++){if((d=a._il[b])&&"s_l"==d._c&&d.tagContainerName==c){a.Q(d);if(d.lmq){for(b=0;b<d.lmq.length;b++){f=d.lmq[b],a.loadModule(f.n)}}if(d.ml){for(f in d.ml){if(a[f]){for(b in c=a[f],f=d.ml[f],f){!Object.prototype[b]&&("function"!=typeof f[b]||0>(""+f[b]).indexOf("s_c_il"))&&(c[b]=f[b])}}}}if(d.mmq){for(b=0;b<d.mmq.length;b++){f=d.mmq[b],a[f.m]&&(c=a[f.m],c[f.f]&&"function"==typeof c[f.f]&&(f.a?c[f.f].apply(c,f.a):c[f.f].apply(c)))}}if(d.tq){for(b=0;b<d.tq.length;b++){a.track(d.tq[b])}}d.s=a;break}}};a.Util={urlEncode:a.escape,urlDecode:a.unescape,cookieRead:a.cookieRead,cookieWrite:a.cookieWrite,getQueryParam:function(c,b,d){var f;b||(b=a.pageURL?a.pageURL:k.location);d||(d="&");return c&&b&&(b=""+b,f=b.indexOf("?"),0<=f&&(b=d+b.substring(f+1)+d,f=b.indexOf(d+c+"="),0<=f&&(b=b.substring(f+d.length+c.length+1),f=b.indexOf(d),0<=f&&(b=b.substring(0,f)),0<b.length)))?a.unescape(b):""}};a.G="supplementalDataID timestamp dynamicVariablePrefix visitorID marketingCloudVisitorID analyticsVisitorID audienceManagerLocationHint authState fid vmk visitorMigrationKey visitorMigrationServer visitorMigrationServerSecure charSet visitorNamespace cookieDomainPeriods fpCookieDomainPeriods cookieLifetime pageName pageURL referrer contextData currencyCode lightProfileID lightStoreForSeconds lightIncrementBy retrieveLightProfiles deleteLightProfiles retrieveLightData".split(" ");a.g=a.G.concat("purchaseID variableProvider channel server pageType transactionID campaign state zip events events2 products audienceManagerBlob tnt".split(" "));a.ma="timestamp charSet visitorNamespace cookieDomainPeriods cookieLifetime contextData lightProfileID lightStoreForSeconds lightIncrementBy".split(" ");a.O=a.ma.slice(0);a.wa="account allAccounts debugTracking visitor visitorOptedOut trackOffline offlineLimit offlineThrottleDelay offlineFilename usePlugins doPlugins configURL visitorSampling visitorSamplingGroup linkObject clickObject linkURL linkName linkType trackDownloadLinks trackExternalLinks trackClickMap trackInlineStats linkLeaveQueryString linkTrackVars linkTrackEvents linkDownloadFileTypes linkExternalFilters linkInternalFilters useForcedLinkTracking forcedLinkTrackingTimeout trackingServer trackingServerSecure ssl abort mobile dc lightTrackVars maxDelay expectSupplementalData usePostbacks AudienceManagement".split(" ");for(n=0;250>=n;n++){76>n&&(a.g.push("prop"+n),a.O.push("prop"+n)),a.g.push("eVar"+n),a.O.push("eVar"+n),6>n&&a.g.push("hier"+n),4>n&&a.g.push("list"+n)}n="pe pev1 pev2 pev3 latitude longitude resolution colorDepth javascriptVersion javaEnabled cookiesEnabled browserWidth browserHeight connectionType homepage pageURLRest".split(" ");a.g=a.g.concat(n);a.G=a.G.concat(n);a.ssl=0<=k.location.protocol.toLowerCase().indexOf("https");a.charSet="UTF-8";a.contextData={};a.offlineThrottleDelay=0;a.offlineFilename="AppMeasurement.offline";a.Ka=0;a.la=0;a.N=0;a.Ja=0;a.linkDownloadFileTypes="exe,zip,wav,mp3,mov,mpg,avi,wmv,pdf,doc,docx,xls,xlsx,ppt,pptx";a.w=k;a.d=k.document;try{if(a.Oa=!1,navigator){var y=navigator.userAgent;if("Microsoft Internet Explorer"==navigator.appName||0<=y.indexOf("MSIE ")||0<=y.indexOf("Trident/")&&0<=y.indexOf("Windows NT 6")){a.Oa=!0}}}catch(z){}a.fa=function(){a.ga&&(k.clearTimeout(a.ga),a.ga=q);a.l&&a.J&&a.l.dispatchEvent(a.J);a.A&&("function"==typeof a.A?a.A():a.l&&a.l.href&&(a.d.location=a.l.href));a.l=a.J=a.A=0};a.Ma=function(){a.b=a.d.body;a.b?(a.v=function(c){var b,d,f,e,g;if(!(a.d&&a.d.getElementById("cppXYctnr")||c&&c["s_fe_"+a._in])){if(a.ya){if(a.useForcedLinkTracking){a.b.removeEventListener("click",a.v,!1)}else{a.b.removeEventListener("click",a.v,!0);a.ya=a.useForcedLinkTracking=0;return}}else{a.useForcedLinkTracking=0}a.clickObject=c.srcElement?c.srcElement:c.target;try{if(!a.clickObject||a.M&&a.M==a.clickObject||!(a.clickObject.tagName||a.clickObject.parentElement||a.clickObject.parentNode)){a.clickObject=0}else{var m=a.M=a.clickObject;a.ka&&(clearTimeout(a.ka),a.ka=0);a.ka=setTimeout(function(){a.M==m&&(a.M=0)},10000);f=a.Ea();a.track();if(f<a.Ea()&&a.useForcedLinkTracking&&c.target){for(e=c.target;e&&e!=a.b&&"A"!=e.tagName.toUpperCase()&&"AREA"!=e.tagName.toUpperCase();){e=e.parentNode}if(e&&(g=e.href,a.Ga(g)||(g=0),d=e.target,c.target.dispatchEvent&&g&&(!d||"_self"==d||"_top"==d||"_parent"==d||k.name&&d==k.name))){try{b=a.d.createEvent("MouseEvents")}catch(n){b=new k.MouseEvent}if(b){try{b.initMouseEvent("click",c.bubbles,c.cancelable,c.view,c.detail,c.screenX,c.screenY,c.clientX,c.clientY,c.ctrlKey,c.altKey,c.shiftKey,c.metaKey,c.button,c.relatedTarget)}catch(q){b=0}b&&(b["s_fe_"+a._in]=b.s_fe=1,c.stopPropagation(),c.stopImmediatePropagation&&c.stopImmediatePropagation(),c.preventDefault(),a.l=c.target,a.J=b)}}}}}catch(r){a.clickObject=0}}},a.b&&a.b.attachEvent?a.b.attachEvent("onclick",a.v):a.b&&a.b.addEventListener&&(navigator&&(0<=navigator.userAgent.indexOf("WebKit")&&a.d.createEvent||0<=navigator.userAgent.indexOf("Firefox/2")&&k.MouseEvent)&&(a.ya=1,a.useForcedLinkTracking=1,a.b.addEventListener("click",a.v,!0)),a.b.addEventListener("click",a.v,!1))):setTimeout(a.Ma,30)};a.Ma();a.loadModule("ActivityMap")}function s_gi(a){var k,q=window.s_c_il,r,n,t=a.split(","),u,s,x=0;if(q){for(r=0;!x&&r<q.length;){k=q[r];if("s_c"==k._c&&(k.account||k.oun)){if(k.account&&k.account==a){x=1}else{for(n=k.account?k.account:k.oun,n=k.allAccounts?k.allAccounts:n.split(","),u=0;u<t.length;u++){for(s=0;s<n.length;s++){t[u]==n[s]&&(x=1)}}}}r++}}x||(k=new AppMeasurement);k.setAccount?k.setAccount(a):k.sa&&k.sa(a);return k}AppMeasurement.getInstance=s_gi;window.s_objectID||(window.s_objectID=0);function s_pgicq(){var a=window,k=a.s_giq,q,r,n;if(k){for(q=0;q<k.length;q++){r=k[q],n=s_gi(r.oun),n.setAccount(r.un),n.setTagContainer(r.tagContainerName)}}a.s_giq=0}s_pgicq();
